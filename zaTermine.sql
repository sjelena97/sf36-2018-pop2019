﻿DROP TABLE IF EXISTS Termini

CREATE TABLE [dbo].[Termini] (
[Sifra] INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
[TipNastave] VARCHAR(20),
[Dan] VARCHAR(15) NOT NULL, 
[TerminOd] SMALLDATETIME NOT NULL, 
[TerminDo] SMALLDATETIME NOT NULL,
[SifraUcionice] INT NOT NULL,
[KorisnickoImeKorisnika] VARCHAR(50) NOT NULL,
[Active] BIT NOT NULL,
CONSTRAINT Fk_Ucionica_Termin_Sifra FOREIGN KEY (SifraUcionice)
	REFERENCES dbo.Ucionice (Sifra),
CONSTRAINT Fk_Korisnik_Termin_KorisnickoImeKorisnika FOREIGN KEY (KorisnickoImeKorisnika)
	REFERENCES dbo.Korisnici (KorisnickoIme),
)

insert into [dbo].[Termini](TipNastave, Dan, TerminOd, TerminDo, SifraUcionice, KorisnickoImeKorisnika, Active) values ('Predavanja','Ponedjeljak','2020-01-28 12:30:00','2020-01-28 16:30:00', 3003, 'petarp', 1)
insert into [dbo].[Termini](TipNastave, Dan, TerminOd, TerminDo, SifraUcionice, KorisnickoImeKorisnika, Active) values ('Vjezbe','Utorak','2020-01-30 16:00:00','2020-01-30 18:00:00', 1, 'sasa', 1)

