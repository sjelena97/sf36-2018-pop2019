﻿CREATE TABLE [dbo].[Ustanova]
(
	[Sifra] INT IDENTITY(1,1) PRIMARY KEY, 
	[Naziv] VARCHAR(50) NOT NULL UNIQUE,
	[Lokacija] VARCHAR(50),
	[Active] BIT NOT NULL

)

CREATE TABLE [dbo].[Ucionice]
(
	[Sifra] INT IDENTITY(1,1) PRIMARY KEY, 
	[BrojUcionice] VARCHAR(10) NOT NULL,
	[BrojMesta] INT,
	[TipUcionice] VARCHAR (20) NOT NULL,
	[SifraUstanove] INT,
	CONSTRAINT Fk_Ustanova_Ucionica_Sifra FOREIGN KEY (SifraUstanove)
	REFERENCES dbo.Ustanova (sifra),
	[Active] BIT NOT NULL

)