﻿DROP TABLE IF EXISTS Ucionice
DROP TABLE IF EXISTS Ustanova

CREATE TABLE [dbo].[Ustanova](
[Sifra] int not null identity(1,1) primary key,
[Naziv] varchar(50) not null,
[Lokacija] varchar(50) not null,
[Active] bit not null
)