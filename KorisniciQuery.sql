﻿DROP TABLE IF EXISTS Korisnici

CREATE TABLE [dbo].[Korisnici] (
	[KorisnickoIme] VARCHAR(50) PRIMARY KEY NOT NULL, 
	[Ime] VARCHAR (20) NOT NULL,
	[Prezime] VARCHAR (30) NOT NULL, 
	[Email] VARCHAR (50) NOT NULL,
	[Lozinka] VARCHAR (50) NOT NULL,
	[TipKorisnika] VARCHAR (15) NOT NULL,
	[SifraUstanove] INT NULL,
	[KImeProfesor] VARCHAR(50) NULL,
	[Active]  BIT NOT NULL
)

insert into [dbo].[Korisnici](KorisnickoIme, Ime, Prezime, Email, Lozinka, TipKorisnika, Active) 
values('admin' ,'Marko', 'Markovic', 'admin@gmail.com','admin', 'Administrator' , 1);

insert into [dbo].[Korisnici](KorisnickoIme, Ime, Prezime, Email, Lozinka, TipKorisnika, Active, SifraUstanove) 
values('petarp' ,'Petar', 'Petrovic', 'petarp@gmail.com', 'petar123', 'Profesor' , 1, 1);

insert into [dbo].[Korisnici](KorisnickoIme, Ime, Prezime, Email, Lozinka, TipKorisnika, Active, SifraUstanove, KImeProfesor) 
values('sasa' ,'Sasa', 'Tomic', 'sasatomic@gmail.com', 'sasa123', 'Asistent' , 1, 1, 'petarp');


