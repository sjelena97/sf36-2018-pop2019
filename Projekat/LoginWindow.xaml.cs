﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LogInBtn_Click(object sender, RoutedEventArgs e)
        {
            UstanovaDao ustanovaDAO = new UstanovaDao();
            List<Ustanova> ustanove = ustanovaDAO.SelectUstanova();
            Util.Ustanove = ustanove;

            KorisnikDAO korisnikDAO = new KorisnikDAO();
            List<Korisnik> korisnici = korisnikDAO.SelectKorisnici();
            Util.Korisnici = korisnici;

            Util.GetProfesori();
            Util.GetAsistenti();

            String korisnickoIme = KorisnickoIme.Text;
            String lozinka = Lozinka.Password.ToString();
            Util.prijavljeniKorisnik = Util.LogIn(korisnickoIme, lozinka);
            Korisnik korisnik = Util.prijavljeniKorisnik;
            
            if (korisnik == null)
            {
                MessageBox.Show($"Pogresni podaci!", "Greska", MessageBoxButton.OK);
                KorisnickoIme.Text = "";
                Lozinka.Password = "";
                return;
            }else if(korisnik.TipKorisnika.Equals(TipKorisnika.Administrator))
            {
                Close();
                UstanoveWindow ustanoveWindow = new UstanoveWindow();
                ustanoveWindow.ShowDialog();
            }
            else if (korisnik.TipKorisnika.Equals(TipKorisnika.Profesor) || korisnik.TipKorisnika.Equals(TipKorisnika.Asistent))
            {
                Close();
                TerminiWindow terminiWindow = new TerminiWindow();
                terminiWindow.ShowDialog();
            }

        }
    }
}
