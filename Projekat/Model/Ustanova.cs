﻿using Projekat.DAO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat.Model

{   [Serializable]
    public class Ustanova
    {
        public int sifra { get; set; }
        public String naziv { get; set; }
        public String lokacija { get; set; }

        private ObservableCollection<Ucionica> ucionice;

        public ObservableCollection<Ucionica> Ucionice
        {
            get { return ucionice; }
            set { ucionice = value; }
        }
        public bool Active { get; set; } = true;

        public Ustanova() {
            sifra = UstanovaDao.SifraZaUstanovu(); 
            naziv = "";
            lokacija = "";
            this.Ucionice = new ObservableCollection<Ucionica>();
            Active = true;
        }

        public Ustanova(Ustanova original)
        {
            sifra = original.sifra;
            naziv = original.naziv;
            lokacija = original.lokacija;
            ucionice = original.ucionice;
            Active = original.Active;
            this.Ucionice = new ObservableCollection<Ucionica>();
        }

        public Ustanova(int sifra, string naziv, string lokacija, bool active)
        {
            this.sifra = sifra;
            this.naziv = naziv;
            this.lokacija = lokacija;
            this.Active = active;
            this.ucionice = new ObservableCollection<Ucionica>();
        }
    }
}
