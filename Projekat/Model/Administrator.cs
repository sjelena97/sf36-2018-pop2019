﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe.Model;

namespace Projekat.Model
{
    public class Administrator : Korisnik
    {
        public Administrator()
        {
            Ime = "";
            Prezime = "";
            Email = "";
            KorisnickoIme = "";
            Lozinka = "";
            TipKorisnika = TipKorisnika.Administrator;
            Active = true;
        }
        public Administrator(string ime, string prezime, string email, string korisnickoIme, string lozinka, bool active) : base(ime, prezime, email, korisnickoIme, lozinka, active)
        {
            TipKorisnika = TipKorisnika.Administrator;
        }

        public Administrator(Administrator original)
        {
            Ime = original.Ime;
            Prezime = original.Prezime;
            Email = original.Email;
            KorisnickoIme = original.KorisnickoIme;
            Lozinka = original.Lozinka;
            TipKorisnika = TipKorisnika.Administrator;
            Active = original.Active;
        }
    }
}
