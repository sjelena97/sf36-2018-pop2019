﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe.Model;

namespace Projekat.Model
{
    public class Termin
    {
        private String vrijeme;

        private int sifra;
        public int Sifra
        {
            get { return sifra; }
            set { sifra = value; }
        }

        private TipNastave tipNastave;
        public TipNastave TipNastave
        {
            get { return tipNastave; }
            set { tipNastave = value; }
        }

        private Dani dan;
        public Dani Dan
        {
            get { return dan; }
            set { dan = value; }
        }

        private DateTime terminOd;
        public DateTime TerminOd
        {
            get { return terminOd; }
            set { terminOd = value; }
        }

        private DateTime terminDo;
        public DateTime TerminDo
        {
            get { return terminDo; }
            set { terminDo = value; }
        }

        private Ucionica ucionica;
        public Ucionica Ucionica
        {
            get { return ucionica; }
            set { ucionica = value; }
        }

        private Korisnik korisnik;
        public Korisnik Korisnik
        {
            get { return korisnik; }
            set { korisnik = value; }
        }

        private bool active;
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public Termin()
        {
            Sifra = 0;
            TerminOd = new DateTime();
            TerminDo = new DateTime();
            Dan = Dani.Ponedjeljak;
            TipNastave = TipNastave.Predavanja;
            Korisnik = new Korisnik();
            Ucionica = new Ucionica();
            Active = true;
        }

        public Termin(DateTime terminOd, DateTime terminDo, Dani dan, TipNastave tipNastave, Ucionica ucionica, Korisnik korisnik, bool active)
        {
            this.TerminOd= terminOd;
            this.TerminDo = terminDo;
            this.Dan = dan;
            this.TipNastave = tipNastave;
            this.Ucionica = ucionica;
            this.Korisnik = korisnik;
            this.Active = active;
        }

        public Termin(Termin original)
        {
            Sifra = original.Sifra;
            TerminOd = original.TerminOd;
            TerminDo = original.TerminDo;
            Dan = original.Dan;
            tipNastave = original.TipNastave;
            Ucionica = original.Ucionica;
            Korisnik = original.Korisnik;
            Active = original.Active;
        }
    }
}
