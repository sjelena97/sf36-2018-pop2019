﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe.Model;

namespace Projekat.Model
{
    public class Asistent : Korisnik
    {
        private Profesor profesor;
        public Profesor Profesor
        {
            get { return profesor; }
            set { profesor = value; }
        }


        public Asistent() {
            TipKorisnika = TipKorisnika.Asistent;
            profesor = new Profesor();
        }

        public Asistent(Asistent original)
        {
            TipKorisnika = TipKorisnika.Asistent;
            profesor = original.profesor;
        }

        public Asistent(string ime, string prezime, string email, string korisnickoIme, string lozinka, Profesor profesor, bool active) : base(ime, prezime, email, korisnickoIme, lozinka, active)
        {
            this.profesor = profesor;
            TipKorisnika = TipKorisnika.Asistent;
           
        }
    }
}
