﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat.Model

{   [Serializable]
    public class Ucionica
    {
        public Ustanova ustanova { get; set; }
        public int Sifra { get; set; }
        public string BrojUcionice { get; set; }
        public int BrojMesta { get; set; }
        public TipUcionice TipUcionice { get; set; }
        public bool Active { get; set; } = true;

        public ObservableCollection<Termin> termini { get; set; }

        public Ucionica() {
            ustanova = new Ustanova();
            Sifra = 0;
            BrojUcionice = "";
            BrojMesta = 0;
            TipUcionice = TipUcionice.Bezracunarska;
            termini = new ObservableCollection<Termin>();
            this.Active = true;
        }

        public Ucionica(Ucionica original)
        {
            ustanova = original.ustanova;
            Sifra = original.Sifra;
            BrojUcionice = original.BrojUcionice;
            BrojMesta = original.BrojMesta;
            TipUcionice = original.TipUcionice;
            termini = new ObservableCollection<Termin>();
            Active = original.Active;
        }

        public Ucionica(int sifra, string brojUcionice, int brojMesta, TipUcionice tipUcionice, Ustanova ustanova, bool active)
        {
            this.Sifra = sifra;
            this.BrojUcionice = brojUcionice;
            this.BrojMesta = brojMesta;
            this.TipUcionice = tipUcionice;
            this.ustanova = ustanova;
            this.Active = active;
        }
    }
}
