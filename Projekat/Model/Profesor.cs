﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe.Model;

namespace Projekat.Model
{
    public class Profesor : Korisnik
    {
        private ObservableCollection<Korisnik> asistenti;

        public ObservableCollection<Korisnik> Asistenti
        {
            get { return asistenti; }
            set { asistenti = value; }
        }


        public Profesor() {
            TipKorisnika = TipKorisnika.Profesor;
            this.asistenti = new ObservableCollection<Korisnik>();
        }
        public Profesor(string ime, string prezime, string email, string korisnickoIme, string lozinka, bool active) : base(ime, prezime, email, korisnickoIme, lozinka, active)
        {
            TipKorisnika = TipKorisnika.Profesor;
            this.asistenti = new ObservableCollection<Korisnik>();
        }

        public Profesor(Profesor original)
        {
            TipKorisnika = TipKorisnika.Profesor;
            Asistenti = original.Asistenti;
        }
    }
}
