﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbe.Model
{
    public enum TipKorisnika
    {
        Administrator, Asistent, Profesor
    }
}
