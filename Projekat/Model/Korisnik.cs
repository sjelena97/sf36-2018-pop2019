﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbe.Model

{   [Serializable]
    public class Korisnik
    {
        private string ime;
        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        private string prezime;
        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string korisnickoIme;
        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; }
        }

        private string lozinka;
        public string Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; }
        }

        private TipKorisnika tipKorisnika;
        public TipKorisnika TipKorisnika

        {
            get { return tipKorisnika; }
            set { tipKorisnika = value; }
        }

        private bool active;
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }


        private Ustanova ustanova;
        public Ustanova Ustanova
        {
            get { return ustanova; }
            set { ustanova = value; }
        }

        private ObservableCollection<Termin> termini;

        public ObservableCollection<Termin> Termini
        {
            get { return termini; }
            set { termini = value; }
        }


        public Korisnik() {
            Ime = "";
            Prezime = "";
            Email = "";
            KorisnickoIme = "";
            Lozinka = "";
            Active = true;
            Termini = new ObservableCollection<Termin>();
        }

        public Korisnik(string ime, string prezime, string mail, string korisnickoIme, string lozinka, bool active)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.Email = mail;
            this.KorisnickoIme = korisnickoIme;
            this.Lozinka = lozinka;
            this.Active = active;
            Termini = new ObservableCollection<Termin>();
        }

        public Korisnik(Korisnik original)
        {
            Ime = original.Ime;
            Prezime = original.Prezime;
            Email = original.Email;
            KorisnickoIme = original.KorisnickoIme;
            Lozinka = original.Lozinka;
            TipKorisnika = original.TipKorisnika;
            Active = original.Active;
        }

    }

}
