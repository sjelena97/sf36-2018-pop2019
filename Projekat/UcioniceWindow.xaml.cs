﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for UcioniceWindow.xaml
    /// </summary>
    public partial class UcioniceWindow : Window
    {
        public ICollectionView CollectionView;
        public Ustanova ustanovaUcionice;
        public UcioniceWindow(Ustanova ustanova)
        {
            InitializeComponent();
            ubaciParametreUCombo();
            ustanovaUcionice = ustanova;
            CollectionView = CollectionViewSource.GetDefaultView(ustanova.Ucionice);
            ustanovaUcionice = ustanova;
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;

        }

        private bool ActiveFilter(object obj)
        {
            var ucionica = obj as Ucionica;
            return ucionica.Active;
        }

        public void AddUcionicaBtn_Click(object sender, RoutedEventArgs e)
        {
            Ucionica novaUcionica = new Ucionica();
            novaUcionica.ustanova = ustanovaUcionice;
            AddEditUcionicaWindow addEditUcionicaWindow = new AddEditUcionicaWindow(novaUcionica);
            addEditUcionicaWindow.ShowDialog();
            bool ucionicaAdded = (bool)addEditUcionicaWindow.DialogResult;
            if (ucionicaAdded)
            {
                UcionicaDAO.SifraZaUcionicu(ustanovaUcionice);
                UcionicaDAO ucionicaDao = new UcionicaDAO();
                ucionicaDao.CreateUcionica(addEditUcionicaWindow.Ucionica, ustanovaUcionice);
                ucionicaDao.SelectUcionica(addEditUcionicaWindow.Ucionica.ustanova);
                CollectionView.Refresh();
            }
        }

        private void EditUcionicaBtn_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = UcioniceGrid.SelectedItem as Ucionica;
            if (ucionica == null)
            {
                MessageBox.Show("Niste odabrali ucionicu", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ucionica ucionicaCopy = new Ucionica(ucionica);
                AddEditUcionicaWindow editUcionicaWindow = new AddEditUcionicaWindow(ucionica, AddEditUcionicaWindow.UcionicaWindowState.Editing);
                editUcionicaWindow.ShowDialog();

                bool shouldEditUcionica = editUcionicaWindow.DialogResult.Value;

                if (shouldEditUcionica == false)
                {
                    int indx = 0;
                    if (ucionica.Sifra == ucionicaCopy.Sifra)
                    {
                        indx = ustanovaUcionice.Ucionice.IndexOf(ucionica);
                    }
                    ustanovaUcionice.Ucionice[indx] = ucionicaCopy;
                }
                UcionicaDAO ucionicaDao = new UcionicaDAO();
                ucionicaDao.UpdateUcionica(editUcionicaWindow.Ucionica);

                //              Util.GetInstance().UpdateUcionica(ucionica, ucionicaCopy);
                CollectionView.Refresh();
            }
        }

        private void DeleteUcionicaBtn_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionicaZaBrisanje = UcioniceGrid.SelectedItem as Ucionica;
            if (ucionicaZaBrisanje == null)
            {
                MessageBox.Show("Niste odabrali ucionicu", "Warning", MessageBoxButton.OK);
            }
            else
            {

                MessageBoxResult result = MessageBox.Show("Da li zelite obrisati ucionicu " + ucionicaZaBrisanje.BrojUcionice + "?", "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        if(ucionicaZaBrisanje.termini != null)
                        { 
                            foreach (Termin t in ucionicaZaBrisanje.termini)
                            {
                                TerminDAO terminDao = new TerminDAO();
                                terminDao.DeleteTermini(t, ucionicaZaBrisanje);
                            }
                        }
                        UcionicaDAO ucionicaDao = new UcionicaDAO();
                        ucionicaDao.DeleteUcionica(ucionicaZaBrisanje);
                        MessageBox.Show("Uspesno ste obrisali ucionicu", "Message");
                        //                    Util.GetInstance().Ustanove.Remove(ustanovaZaBrisanje);
                        CollectionView.Refresh();
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                }
            }
        }

        public void TerminiBtn_Click(object sender, RoutedEventArgs e)
        {
            var odabranaUcionica = UcioniceGrid.SelectedItem as Ucionica;
            if (odabranaUcionica == null)
            {
                MessageBox.Show("Niste odabrali ucionicu", "Warning", MessageBoxButton.OK);
            }
            else
            {

                TerminiWindow WindowTermini = new TerminiWindow(odabranaUcionica);
                WindowTermini.Show();
            }

        }

        private void PretragaKeyUp(object sender, KeyEventArgs e)
        {
            String parametar = "";
            if (ComboPretraga.SelectedItem != null)
            {
                parametar = ComboPretraga.SelectedItem.ToString();
            }
            UcionicaDAO.PretragaUcionica(txtPretraga.Text, parametar, ustanovaUcionice);
            CollectionView.Refresh();
        }
        public void ubaciParametreUCombo()
        {
            ComboPretraga.Items.Add("Broj ucionice");
            ComboPretraga.Items.Add("Broj mesta");
            ComboPretraga.Items.Add("Tip ucionice");
        }

    }
}
