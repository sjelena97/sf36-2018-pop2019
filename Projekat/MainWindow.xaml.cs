﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
 //     ObservableCollection<Korisnik> korisnici = new ObservableCollection<Korisnik>(Util.GetInstance().Korisnici);

        public ICollectionView CollectionView;
        Profesor profesor = new Profesor();
        public MainWindow()
        {
            InitializeComponent();
            ubaciParametreUCombo();
            if(Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Profesor"))
            {
                foreach (Profesor p in Util.Profesori)
                {
                    if (p.KorisnickoIme.Equals(Util.prijavljeniKorisnik.KorisnickoIme))
                    {
                        profesor = p;
                    }
                }
                AddUserBtn.Visibility = Visibility.Hidden;
                EditUserBtn.Visibility = Visibility.Hidden;
                DeleteUserBtn.Visibility = Visibility.Hidden;
                CollectionView = CollectionViewSource.GetDefaultView(profesor.Asistenti);
            }
            else
            {
                AddAsistentBtn.Visibility = Visibility.Hidden;
                DeleteAsistentBtn.Visibility = Visibility.Hidden;
                CollectionView = CollectionViewSource.GetDefaultView(Util.Korisnici);
            }
    
            CollectionView.Filter = ActiveFilter;
            //         DataContext = korisnici;
            //         Refresh();
            DataContext = CollectionView;
             
        }

        private bool ActiveFilter(object obj)
        {
            var user = obj as Korisnik;
            return user.Active;
        }



        //       private void Refresh()
        //       {
        //           UsersGrid.ItemsSource = null;
        //           UsersGrid.ItemsSource = Util.GetInstance().Korisnici;
        //       }

        private void AddUserBtn_Click(object sender, RoutedEventArgs e)
        {
            Korisnik noviAdmin = new Korisnik();
            AddEditUserWindow addEditUserWindow = new AddEditUserWindow(noviAdmin, "dodavanje");
            addEditUserWindow.ShowDialog();
            bool adminAdded = (bool)addEditUserWindow.DialogResult;
            if (adminAdded)
            { 
                Util.Korisnici.Add(addEditUserWindow.Korisnik);
                KorisnikDAO korisnikDao = new KorisnikDAO();
                if (addEditUserWindow.Korisnik.TipKorisnika.Equals(TipKorisnika.Administrator))
                {
                    korisnikDao.CreateKorisnik(addEditUserWindow.Korisnik);
                }else if (addEditUserWindow.Korisnik.TipKorisnika.Equals(TipKorisnika.Profesor))
                {
                    korisnikDao.CreateProfesor(addEditUserWindow.Profesor);
                }else if (addEditUserWindow.Korisnik.TipKorisnika.Equals(TipKorisnika.Asistent))
                {
                    korisnikDao.CreateAsistent(addEditUserWindow.Asistent);
                }
               
                
                CollectionView.Refresh();
                //korisnici.Add(addEditUserWindow.Korisnik);
            }
            //         Refresh

        }

        private void EditUserBtn_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = UsersGrid.SelectedItem as Korisnik;
            if (korisnik == null)
            {
                MessageBox.Show("Niste odabrali korisnika", "Warning", MessageBoxButton.OK);
            }
            {

                Korisnik userCopy = new Korisnik(korisnik);
                AddEditUserWindow editUserWindow = new AddEditUserWindow(korisnik, "", AddEditUserWindow.UserWindowState.Editing);
                editUserWindow.ShowDialog();

                bool shouldEditUser = editUserWindow.DialogResult.Value;

                if (shouldEditUser == false)
                {
                    int indeks = Util.Korisnici.FindIndex(k => k.KorisnickoIme.Equals(userCopy.KorisnickoIme));
                    Util.Korisnici[indeks] = userCopy;

                }
                KorisnikDAO korisnikDao = new KorisnikDAO();
                korisnikDao.UpdateKorisnika(editUserWindow.Korisnik);
                CollectionView.Refresh();
            }
            
        }

        private void DeleteUserBtn_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnikZaBrisanje = UsersGrid.SelectedItem as Korisnik;
            if (korisnikZaBrisanje == null)
            {
                MessageBox.Show("Niste odabrali korisnika", "Warning", MessageBoxButton.OK);
            }
            else { 

                MessageBoxResult result = MessageBox.Show("Da li zelite obrisati korisnika " + korisnikZaBrisanje.KorisnickoIme + "?" , "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        KorisnikDAO korisnikDao = new KorisnikDAO();
                        if (korisnikZaBrisanje.TipKorisnika.ToString().Equals("Profesor"))
                        {
                            Profesor profesorZaBrisanje = (Profesor)korisnikZaBrisanje;
                            foreach(Asistent asist in Util.Asistenti)
                            {
                                if (asist.Profesor.KorisnickoIme.Equals(profesorZaBrisanje.KorisnickoIme))
                                {
                                    profesorZaBrisanje.Asistenti.Add(asist);
                                }
                            }
                            foreach (Asistent a in profesorZaBrisanje.Asistenti)
                            {
                                korisnikDao.UpdateAsistenti(a, profesorZaBrisanje);
                            }
                            korisnikDao.DeleteProfesor(profesorZaBrisanje);
                        }
                        else
                        {
                            korisnikDao.DeleteKorisnik(korisnikZaBrisanje);
                        }
                        Util.Korisnici.Remove(korisnikZaBrisanje);
                        MessageBox.Show("Uspesno ste obrisali korisnika", "Message");
                        CollectionView.Refresh();
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                }
            }
        }

        private void AddAsistentBtn_Click(object sender, RoutedEventArgs e)
        {
            DodijeliAsistentaWindow dodijeliAsistentaWindow = new DodijeliAsistentaWindow();
            dodijeliAsistentaWindow.ShowDialog();

            KorisnikDAO korisnikDao = new KorisnikDAO();
            List<Korisnik> korisnici = korisnikDao.SelectKorisnici();
            Util.Korisnici = korisnici;
            Util.GetProfesori();
            Util.GetAsistenti();
            profesor.Asistenti.Add(dodijeliAsistentaWindow.Asistent);

            CollectionView.Refresh();

        }

        private void DeleteAsistentBtn_Click(object sender, RoutedEventArgs e)
        {
            Asistent asistentZaUkloniti = UsersGrid.SelectedItem as Asistent;
            if (asistentZaUkloniti == null)
            {
                MessageBox.Show("Niste odabrali asistenta", "Warning", MessageBoxButton.OK);
            }
            else
            {

                MessageBoxResult result = MessageBox.Show("Da li zelite ukloniti asistenta " + asistentZaUkloniti.KorisnickoIme + "?", "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        KorisnikDAO korisnikDao = new KorisnikDAO();
                        korisnikDao.UkloniAsistenta(asistentZaUkloniti);

                        List<Korisnik> korisnici = korisnikDao.SelectKorisnici();
                        Util.Korisnici = korisnici;
                        Util.GetProfesori();
                        Util.GetAsistenti();
                        profesor.Asistenti.Remove(asistentZaUkloniti);
                        MessageBox.Show("Uspesno ste uklonili asistenta", "Message");
                        CollectionView.Refresh();
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                }
            }
        }

        private void PretragaKeyUp(object sender, KeyEventArgs e)
        {
            String parametar = "";
            if (ComboPretraga.SelectedItem != null)
            {
                parametar = ComboPretraga.SelectedItem.ToString();
            }
            if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Administrator"))
            {
                KorisnikDAO.PretragaKorisnika(txtPretraga.Text, parametar, Util.Korisnici);
                CollectionView.Refresh();
            }else if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Profesor"))
            {
                KorisnikDAO.PretragaAsistenata(txtPretraga.Text, parametar, profesor);
                CollectionView.Refresh();
            }
        }
        public void ubaciParametreUCombo()
        {
            ComboPretraga.Items.Add("Korisnicko ime");
            ComboPretraga.Items.Add("Ime");
            ComboPretraga.Items.Add("Prezime");
            ComboPretraga.Items.Add("Email");
            ComboPretraga.Items.Add("Tip korisnika");
        }
    }
}
