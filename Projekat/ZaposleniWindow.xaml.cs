﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for ZaposleniWindow.xaml
    /// </summary>
    public partial class ZaposleniWindow : Window
    {
        public ICollectionView CollectionView;
        public Ustanova ustanovaZaposlenih;
        public string imeUstanove;
        public List<Korisnik> zaposleni;
        public ZaposleniWindow(Ustanova ustanova)
        {
            InitializeComponent();
            ubaciParametreUCombo();
            ustanovaZaposlenih = ustanova;
            zaposleni = Util.GetZaposleni(ustanovaZaposlenih);
            CollectionView = CollectionViewSource.GetDefaultView(zaposleni);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;

        }

        private void PretragaKeyUp(object sender, KeyEventArgs e)
        {
            String parametar = "";
            if (ComboPretraga.SelectedItem != null)
            {
                parametar = ComboPretraga.SelectedItem.ToString();
            }
            KorisnikDAO.PretragaZaposlenih(txtPretraga.Text, parametar, zaposleni , ustanovaZaposlenih);
            CollectionView.Refresh();

        }
        public void ubaciParametreUCombo()
        {
            ComboPretraga.Items.Add("Ime");
            ComboPretraga.Items.Add("Prezime");
            ComboPretraga.Items.Add("Email");
            ComboPretraga.Items.Add("Tip korisnika");
        }


        private bool ActiveFilter(object obj)
        {
            var zaposleni = obj as Korisnik;
            return zaposleni.Active;
        }
    }

}
