﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for TerminiWindow.xaml
    /// </summary>
    public partial class TerminiWindow : Window
    {
        public ICollectionView CollectionVieww;
        public Ucionica ucionicaTermina;
        public Ustanova ustanova;
        public Korisnik prijavljeni;
        public ObservableCollection<Termin> terminiKorisnikaa;
        public TerminiWindow()
        {
            if (terminiKorisnikaa != null)
            {
                terminiKorisnikaa.Clear();
            }
            
            InitializeComponent();
            ubaciParametreUCombo();

            ComboPretraga.Items.Remove("Korisnik");
            ustanova = Util.prijavljeniKorisnik.Ustanova;
            prijavljeni = Util.prijavljeniKorisnik;
            terminiKorisnikaa = Util.prijavljeniKorisnik.Termini;
            CollectionVieww = CollectionViewSource.GetDefaultView(Util.prijavljeniKorisnik.Termini);

            CollectionVieww.Filter = ActiveFilter;
            DataContext = CollectionVieww;

            EditTerminBtn.Visibility = Visibility.Hidden;
            if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Asistent"))
            {
                MojiAsistentiBtn.Visibility = Visibility.Hidden;
            }


        }

        public TerminiWindow(Ucionica ucionica)
        {
            InitializeComponent();
            ubaciParametreUCombo();
            ustanova = Util.prijavljeniKorisnik.Ustanova;
            ucionicaTermina = ucionica;
            CollectionVieww = CollectionViewSource.GetDefaultView(ucionica.termini);
            CollectionVieww.Filter = ActiveFilter;
            DataContext = CollectionVieww;


            MojiAsistentiBtn.Visibility = Visibility.Hidden;
            MojProfilBtn.Visibility = Visibility.Hidden;
  

        }


        private bool ActiveFilter(object obj)
        {
            var termin = obj as Termin;
            return termin.Active;
        }

        private void AddTerminBtn_Click(object sender, RoutedEventArgs e)
        {
            Termin noviTermin = new Termin();
            AddEditTerminWindow addEditTerminWindow;
            if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Administrator"))
            {
                noviTermin.Ucionica = ucionicaTermina;
                addEditTerminWindow = new AddEditTerminWindow(noviTermin, ucionicaTermina);
                addEditTerminWindow.ShowDialog();

            }
            else
            {
                addEditTerminWindow = new AddEditTerminWindow(noviTermin, ustanova);
                addEditTerminWindow.ShowDialog();
            }
            bool terminAdded = (bool)addEditTerminWindow.DialogResult;
            if (terminAdded)
            {
                TerminDAO terminDao = new TerminDAO();
                terminDao.CreateTermin(addEditTerminWindow.Termin, ucionicaTermina);
 //               terminDao.SelectUcionica(addEditTerminWindow.Ucionica.ustanova);
                CollectionVieww.Refresh();
            }


        }

        private void EditTerminBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DeleteTerminBtn_Click(object sender, RoutedEventArgs e)
        {
            Termin terminZaBrisanje = TerminiGrid.SelectedItem as Termin;
            if (terminZaBrisanje == null)
            {
                MessageBox.Show("Niste odabrali termin", "Warning", MessageBoxButton.OK);
            }
            else
            {

                MessageBoxResult result = MessageBox.Show("Da li zelite obrisati termin " + terminZaBrisanje.TipNastave.ToString() + " za dan: " + terminZaBrisanje.Dan.ToString() + "?", "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        TerminDAO terminDao = new TerminDAO();
                        terminDao.DeleteTermin(terminZaBrisanje);
                        MessageBox.Show("Uspesno ste obrisali termin", "Message");
                        CollectionVieww.Refresh();
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                }
            }

        }

        private void MojProfilBtn_Click(object sender, RoutedEventArgs e)
        {
            AddEditUserWindow addEditUserWindow = new AddEditUserWindow(Util.prijavljeniKorisnik, "profil");
            addEditUserWindow.ShowDialog();

        }

        private void MojiAsistentiBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.ShowDialog();

        }

        private void RasporedBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
            
            RasporedWindow rasporedWindow = new RasporedWindow(prijavljeni);
            rasporedWindow.ShowDialog();
        }

        private void PretragaKeyUp(object sender, KeyEventArgs e)
        {
            String parametarPretrage = "";
            if (ComboPretraga.SelectedItem != null)
            {
                parametarPretrage = ComboPretraga.SelectedItem.ToString();
            }
            if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Administrator"))
            {
                TerminDAO.PretragaTermina(txtPretraga.Text, parametarPretrage, ucionicaTermina);
                CollectionVieww.Refresh();
            }
            else
            {
                TerminDAO.PretragaTermina(txtPretraga.Text, parametarPretrage, Util.prijavljeniKorisnik);
                CollectionVieww.Refresh();
            }
            
        }
        public void ubaciParametreUCombo()
        {
            ComboPretraga.Items.Add("Tip nastave");
            ComboPretraga.Items.Add("Dan");
            ComboPretraga.Items.Add("Termin od");
            ComboPretraga.Items.Add("Termin do");
            ComboPretraga.Items.Add("Korisnik");
        }

    }
}
