﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for AddEditUstanovaWindow.xaml
    /// </summary>
    public partial class AddEditUstanovaWindow : Window
    {
        public Ustanova Ustanova;

        public enum UstanovaWindowState { Addition, Editing }
        private UstanovaWindowState state;
        public AddEditUstanovaWindow(Ustanova ustanova, UstanovaWindowState ustanovaWindowState = UstanovaWindowState.Addition)
        {
            InitializeComponent();

            Ustanova = ustanova;
            state = ustanovaWindowState;

            DataContext = Ustanova;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void SaveUstanovaBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Validacija() == false)
            {
                return;
            }
            DialogResult = true;
            Close();
        }

        public bool Validacija()
        {
            if (Naziv.Text.Equals("") || Lokacija.Text.Equals(""))
            {
                MessageBox.Show($"Niste popunili sva polja!", "Warning", MessageBoxButton.OK);
                return false;
            }
            return true;
        }
    }
}
