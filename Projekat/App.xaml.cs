﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {

            UstanovaDao ustanovaDAO = new UstanovaDao();
            List<Ustanova> ustanove = ustanovaDAO.SelectUstanova();
            Util.Ustanove = ustanove;

            KorisnikDAO korisnikDAO = new KorisnikDAO();
            List<Korisnik> korisnici = korisnikDAO.SelectKorisnici();
            Util.Korisnici = korisnici;

            Util.GetProfesori();
            Util.GetAsistenti();


            /*          Vezbe.Util.GetInstance().initKorisnici();

                        Ustanova ustanova1 = new Ustanova() { naziv = "Pravni", lokacija = "Novi Sad", Active=true };
                        UstanovaDao ustanovaDao = new UstanovaDao();
                        ustanova1.sifra = ustanovaDao.CreateUstanova(ustanova1);

                        Ucionica ucionica1 = new Ucionica() { BrojUcionice = "1", BrojMesta = 10, TipUcionice=TipUcionice.Racunarska, SifraUstanove=1, Active = true };
                        UcionicaDAO ucionicaDao = new UcionicaDAO();
                        ucionica1.Sifra = ucionicaDao.CreateUcionica(ucionica1);*/

            BasGlavniWindow glavniWindow = new BasGlavniWindow();
            glavniWindow.Show();
        }
    }
}
