﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for UstanoveWindow.xaml
    /// </summary>
    public partial class UstanoveWindow : Window
    {
        public ICollectionView CollectionView;
        public UstanoveWindow()
        {
            InitializeComponent();
            ubaciParametreUCombo();
            CollectionView = CollectionViewSource.GetDefaultView(Util.Ustanove);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;

            if (Util.prijavljeniKorisnik.KorisnickoIme.Equals(""))
            {
                AddUstanovaBtn.Visibility = Visibility.Hidden;
                EditUstanovaBtn.Visibility = Visibility.Hidden;
                DeleteUstanovaBtn.Visibility = Visibility.Hidden;
                UcioniceBtn.Visibility = Visibility.Hidden;
                KorisniciBtn.Visibility = Visibility.Hidden;
                MojProfilBtn.Visibility = Visibility.Hidden;
            }
            else
            {
                ZaposleniBtn.Visibility = Visibility.Hidden;
            }

        }

        private bool ActiveFilter(object obj)
        {
            var ustanova = obj as Ustanova;
            return ustanova.Active;
        }


        private void AddUstanovaBtn_Click(object sender, RoutedEventArgs e)
        {
            Ustanova novaUstanova = new Ustanova();
            AddEditUstanovaWindow addEditUstanovaWindow = new AddEditUstanovaWindow(novaUstanova);
            addEditUstanovaWindow.ShowDialog();
            bool ustanovaAdded = (bool)addEditUstanovaWindow.DialogResult;
            if (ustanovaAdded)
            { 
                Util.Ustanove.Add(addEditUstanovaWindow.Ustanova);
                UstanovaDao ustanovaDao = new UstanovaDao();
                ustanovaDao.CreateUstanova(addEditUstanovaWindow.Ustanova);
                CollectionView.Refresh();
            }

        }

        private void EditUstanovaBtn_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = UstanoveGrid.SelectedItem as Ustanova;
            if (ustanova == null)
            {
                MessageBox.Show("Niste odabrali ustanovu", "Warning", MessageBoxButton.OK);
            }
            else
            {

                Ustanova ustanovaCopy = new Ustanova(ustanova);
                AddEditUstanovaWindow editUstanovaWindow = new AddEditUstanovaWindow(ustanova, AddEditUstanovaWindow.UstanovaWindowState.Editing);
                editUstanovaWindow.ShowDialog();

                bool shouldEditUstanova = editUstanovaWindow.DialogResult.Value;

                if (shouldEditUstanova == false)
                {
                    int indeks = Util.Ustanove.FindIndex(u => u.sifra.Equals(ustanovaCopy.sifra));
                    Util.Ustanove[indeks] = ustanovaCopy;
                    
                }
                UstanovaDao ustanovaDao = new UstanovaDao();
                ustanovaDao.UpdateUstanova(editUstanovaWindow.Ustanova);
                CollectionView.Refresh();
            }
        }

        private void DeleteUstanovaBtn_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanovaZaBrisanje = UstanoveGrid.SelectedItem as Ustanova;
            if (ustanovaZaBrisanje == null)
            {
                MessageBox.Show("Niste odabrali ustanovu", "Warning", MessageBoxButton.OK);
            }
            else
            {

                MessageBoxResult result = MessageBox.Show("Da li zelite obrisati ustanovu " + ustanovaZaBrisanje.naziv + "?", "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        foreach (Ucionica u in ustanovaZaBrisanje.Ucionice)
                        {
                            UcionicaDAO ucionicaDao = new UcionicaDAO();
                            ucionicaDao.DeleteUcionica(u, ustanovaZaBrisanje);
                        }
                        Util.Ustanove.Remove(ustanovaZaBrisanje);
                        UstanovaDao ustanovaDao = new UstanovaDao();
                        ustanovaDao.DeleteUstanova(ustanovaZaBrisanje);
                        MessageBox.Show("Uspesno ste obrisali ustanovu", "Message");
                        //                    Util.GetInstance().Ustanove.Remove(ustanovaZaBrisanje);
                        CollectionView.Refresh();
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                }
            }
        }

        public void UcioniceBtn_Click(object sender, RoutedEventArgs e)
        {

            var odabranaUstanova = UstanoveGrid.SelectedItem as Ustanova;
            if (odabranaUstanova == null)
            {
                MessageBox.Show("Niste odabrali ustanovu", "Warning", MessageBoxButton.OK);
            }
            else
            {
                UcioniceWindow WindowUcionice = new UcioniceWindow(odabranaUstanova);
                WindowUcionice.Show();
            }

        }

        public void ZaposleniBtn_Click(object sender, RoutedEventArgs e)
        {
            var odabranaUstanova = UstanoveGrid.SelectedItem as Ustanova;
            if (odabranaUstanova == null)
            {
                MessageBox.Show("Niste odabrali ustanovu", "Warning", MessageBoxButton.OK);
            }
            else
            {

                ZaposleniWindow WindowZaposleni = new ZaposleniWindow(odabranaUstanova);
                WindowZaposleni.Show();
            }

        }

        public void RasporedBtn_Click(object sender, RoutedEventArgs e)
        {
            var odabranaUstanova = UstanoveGrid.SelectedItem as Ustanova;
            if (odabranaUstanova == null)
            {
                MessageBox.Show("Niste odabrali ustanovu", "Warning", MessageBoxButton.OK);
            }
            else
            {
                RasporedWindow WindowRaspored = new RasporedWindow(odabranaUstanova);
                WindowRaspored.Show();
            }

        }

        public void KorisniciBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.ShowDialog();
        }

        private void MojProfilBtn_Click(object sender, RoutedEventArgs e)
        {
            AddEditUserWindow addEditUserWindow = new AddEditUserWindow(Util.prijavljeniKorisnik, "profil");
            addEditUserWindow.ShowDialog();

        }

        private void PretragaKeyUp(object sender, KeyEventArgs e)
        {
            String parametar = "";
            if(ComboPretraga.SelectedItem != null)
            {
                parametar = ComboPretraga.SelectedItem.ToString();
            }
            UstanovaDao.PretragaUstanova(txtPretraga.Text, parametar, Util.Ustanove);
            CollectionView.Refresh(); 
        }
        public void ubaciParametreUCombo()
        {
            ComboPretraga.Items.Add("Naziv");
            ComboPretraga.Items.Add("Lokacija");
        }

    }
}
