﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for AddEditUcionicaWindow.xaml
    /// </summary>
    public partial class AddEditUcionicaWindow : Window
    {
        public Ucionica Ucionica;

        public enum UcionicaWindowState { Addition, Editing }
        private UcionicaWindowState state;
        public AddEditUcionicaWindow(Ucionica ucionica, UcionicaWindowState ucionicaWindowState = UcionicaWindowState.Addition)
        {
            InitializeComponent();

            Ucionica = ucionica;
            state = ucionicaWindowState;

            TipUcioniceCombo.Items.Add("Racunarska");
            TipUcioniceCombo.Items.Add("Bezracunarska");

            DataContext = Ucionica;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void SaveUcionicaBtn_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(state + " je");
            Ucionica ucionica = Util.PostojiUcionicaUustanovi(BrojUcionice.Text, Ucionica.ustanova);
            if (ucionica != null && state.Equals(UcionicaWindowState.Addition))
            {
                MessageBox.Show($"Ucionica sa brojem {ucionica.BrojUcionice} u odabranoj ustanovi vec postoji", "Greska", MessageBoxButton.OK);
                return;
            }
            ucionica = Util.PostojiUcionicaUustanoviUpdate(BrojUcionice.Text, Ucionica.ustanova, Ucionica);
            if (ucionica != null && state.Equals(UcionicaWindowState.Editing))
            {
                MessageBox.Show($"Ucionica sa brojem {ucionica.BrojUcionice} u odabranoj ustanovi vec postoji", "Greska", MessageBoxButton.OK);
                return;
            }
            if (Validacija() == false)
            {
                return;
            }
            string selectedRole = (string)TipUcioniceCombo.SelectedItem;
            if (selectedRole != null)
            {
                TipUcionice tipUcionice;
                Enum.TryParse(selectedRole, out tipUcionice);

                Ucionica.TipUcionice = tipUcionice;
            }

            DialogResult = true;
            Close();
        }

        public bool Validacija()
        {
            if (BrojMesta.Text.Equals("0"))
            {
                MessageBox.Show($"Polje broj mesta ne moze imati vrednost nula!", "Greska", MessageBoxButton.OK);
                return false;
            }
            string uloga = (string)TipUcioniceCombo.SelectedItem;
            if (BrojUcionice.Text.Equals("") || BrojMesta.Text.Equals("") || uloga == null)
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }
    }
}
