﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for AddEditUserWindow.xaml
    /// </summary>
    public partial class AddEditUserWindow : Window
    {
        public Korisnik Korisnik;
        public Profesor Profesor;
        public Asistent Asistent;

        public enum UserWindowState { Addition, Editing}
        private UserWindowState state;
        public AddEditUserWindow(Korisnik korisnik, String p, UserWindowState userWindowState = UserWindowState.Addition)
        {
            InitializeComponent();

            TipKorisnikaCombo.Items.Add("Administrator");
            TipKorisnikaCombo.Items.Add("Profesor");
            TipKorisnikaCombo.Items.Add("Asistent");
            ubaciUstanoveUCombo();
            ubaciProfesoreUCombo();

            if (p.Equals("profil"))
            {
                Ime.IsEnabled = false;
                Prezime.IsEnabled = false;
                Email.IsEnabled = false;
                Korisnicko_Ime.IsEnabled = false;
                Lozinka.Visibility = Visibility.Hidden;
                LozinkaPrikazi.Visibility = Visibility.Visible;
                LozinkaPrikazi.IsEnabled = false;
                TipKorisnikaCombo.IsEnabled = false;
                TipKorisnikaCombo.Items.Clear();
                TipKorisnikaCombo.Items.Add(korisnik.TipKorisnika.ToString());
                TipKorisnikaCombo.SelectedIndex = 0;
                if (korisnik.TipKorisnika.ToString().Equals("Profesor") || korisnik.TipKorisnika.ToString().Equals("Asistent")){
                    
                    ustanovalbl.Visibility = Visibility.Visible;
                    UstanovaCombo.Visibility = Visibility.Visible;
                    UstanovaCombo.IsEnabled = false;
                    UstanovaCombo.Items.Clear();
                    UstanovaCombo.Items.Add(korisnik.Ustanova.naziv);
                    UstanovaCombo.SelectedIndex = 0;
                }
                SaveUserBtn.Visibility = Visibility.Hidden;
                CancelBtn.Visibility = Visibility.Hidden;

                if (korisnik.TipKorisnika.ToString().Equals("Asistent"))
                {
                    string KImeProf = KorisnikDAO.GetProf(korisnik.KorisnickoIme);
                    Profesor prf = Util.GetProfbyKIme(KImeProf);
                    ProfesorCombo.Items.Clear();
                    profesorlbl.Visibility = Visibility.Visible;
                    ProfesorCombo.Visibility = Visibility.Visible;
                    if (prf == null)
                    {
                        ProfesorCombo.Items.Add("Nije dodijeljen");
                        ProfesorCombo.SelectedIndex = 0;
                    }
                    else
                    {
                        ProfesorCombo.Items.Add(prf.Ime + " " + prf.Prezime);
                        ProfesorCombo.SelectedIndex = 0;
                    }
                    ProfesorCombo.IsEnabled = false;
                }
            }

            state = userWindowState;

            if (state == UserWindowState.Editing)
            {
                Lozinka.Password = korisnik.Lozinka;
                tipKorisnikalbl.Visibility = Visibility.Hidden;
                TipKorisnikaCombo.Visibility = Visibility.Hidden;
                Korisnicko_Ime.IsReadOnly = true;
                ustanovalbl.Visibility = Visibility.Hidden;
                UstanovaCombo.Visibility = Visibility.Hidden;
                profesorlbl.Visibility = Visibility.Hidden;
                ProfesorCombo.Visibility = Visibility.Hidden;
            }

            Korisnik = korisnik;
            this.DataContext = Korisnik;

        }

        public void ubaciUstanoveUCombo()
        {
            foreach (Ustanova ustanova in Util.Ustanove)
            {
                UstanovaCombo.Items.Add(ustanova.naziv);
            }
        }

        public void ubaciProfesoreUCombo()
        {
            ProfesorCombo.Items.Clear();
            foreach (Korisnik korisnik in Util.Korisnici)
            {
                if (UstanovaCombo.SelectedItem != null)
                {
                    Ustanova ustanova = Util.GetUstanovabyNaziv(UstanovaCombo.SelectedItem.ToString());
                    if (korisnik.TipKorisnika == TipKorisnika.Profesor && korisnik.Active == true && korisnik.Ustanova.sifra == ustanova.sifra)
                    {
                        ProfesorCombo.Items.Add(korisnik.KorisnickoIme);
                    }
                }
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void SaveUserBtn_Click(object sender, RoutedEventArgs e)
        {
            if(!(state == UserWindowState.Editing && String.IsNullOrWhiteSpace(Lozinka.Password)))
            {
                Korisnik.Lozinka = Lozinka.Password;
            }
            Korisnik korisnik = Util.PostojiliKorisnickoIme(Korisnicko_Ime.Text);
            if (korisnik != null && state.Equals(UserWindowState.Addition))
            {
                MessageBox.Show($"Korisnik sa korisnickim imenom {korisnik.KorisnickoIme} vec postoji", "Greska", MessageBoxButton.OK);
                return;
            }
 /*           korisnik = Util.PostojiliKorisnickoImeUpdate(Korisnicko_Ime.Text, Korisnik);
            if (korisnik != null && state.Equals(UserWindowState.Editing))
            {
                MessageBox.Show($"Korisnik sa korisnickim imenom {korisnik.KorisnickoIme} vec postoji", "Greska", MessageBoxButton.OK);
                return;
            }*/
            if (state.Equals(UserWindowState.Addition))
            {
                if (Validacija() == false)
                {
                    return;
                }
                if (TipKorisnikaCombo.Text.Equals("Profesor"))
                {
                    Profesor = new Profesor(Korisnik.Ime, Korisnik.Prezime, Korisnik.Email, Korisnik.KorisnickoIme, Korisnik.Lozinka, true);
                    Ustanova ustanova = Util.GetUstanovabyNaziv(UstanovaCombo.SelectedItem.ToString());
                    Profesor.Ustanova = ustanova;

                }
                if (TipKorisnikaCombo.Text.Equals("Asistent"))
                {
                    Profesor profesor = (Profesor)Util.GetProfbyKIme(ProfesorCombo.Text);
                    Asistent = new Asistent(Korisnik.Ime, Korisnik.Prezime, Korisnik.Email, Korisnik.KorisnickoIme, Korisnik.Lozinka, profesor, true);
                    Ustanova ustanova = Util.GetUstanovabyNaziv(UstanovaCombo.SelectedItem.ToString());
                    Asistent.Ustanova = ustanova;
                }
            }
            if (state.Equals(UserWindowState.Editing))
            {
                if (ValidacijaUpdate() == false)
                {
                    return;
                }
            }

            DialogResult = true;
            Close();
        }

        public bool Validacija()
        {
            if (Ime.Text.Equals("") || Prezime.Text.Equals("") || Email.Text.Equals("") || Korisnicko_Ime.Text.Equals("") || Lozinka.Password.Equals("") || UstanovaCombo.SelectedItem == null || TipKorisnikaCombo.SelectedItem == null)
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        public bool ValidacijaUpdate()
        {
            if (Ime.Text.Equals("") || Prezime.Text.Equals("") || Email.Text.Equals("") || Lozinka.Password.Equals(""))
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        private void DropDownClosedTipKorisnikaCombo(object sender, EventArgs e)
        {
            if (TipKorisnikaCombo.Text.Equals("Administrator"))
            {
                ustanovalbl.Visibility = Visibility.Hidden;
                UstanovaCombo.Visibility = Visibility.Hidden;
                profesorlbl.Visibility = Visibility.Hidden;
                ProfesorCombo.Visibility = Visibility.Hidden;
            }
            else if (TipKorisnikaCombo.Text.Equals("Profesor"))
            {
                profesorlbl.Visibility = Visibility.Hidden;
                ProfesorCombo.Visibility = Visibility.Hidden;
                ustanovalbl.Visibility = Visibility.Visible;
                UstanovaCombo.Visibility = Visibility.Visible;
            }
            else if (TipKorisnikaCombo.Text.Equals("Asistent"))
            {
                ustanovalbl.Visibility = Visibility.Visible;
                UstanovaCombo.Visibility = Visibility.Visible;
                profesorlbl.Visibility = Visibility.Visible;
                ProfesorCombo.Visibility = Visibility.Visible;

            }
        }

        private void DropDownClosedUstanovaCombo(object sender, EventArgs e)
        {
            ubaciProfesoreUCombo();
        }
    }
}
