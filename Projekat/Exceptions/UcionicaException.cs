﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat.Exceptions
{
    public class UcionicaException : Exception
    {
        public UcionicaException(string message) : base(message)
        {

        }

    }
}
