﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe;
using Vezbe.Model;

namespace Projekat.DAO
{
    public class KorisnikDAO
    {
        List<Korisnik> korisnici = new List<Korisnik>();

        public List<Korisnik> SelectKorisnici()
        {
            using (SqlConnection connection = new SqlConnection(Util.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Korisnici WHERE Active=1";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Korisnici");

                //                var dataTable = ds.Tables["Ustanova"].AsEnumerable();
                //                dataTable.Where(row => ((string)row["Naziv"]).Contains("FTN")).Count();

                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string Ime = (string)row["Ime"];
                    string Prezime = (string)row["Prezime"];
                    string Email = (string)row["Email"];
                    string KorisnickoIme = (string)row["KorisnickoIme"];
                    string Lozinka = (string)row["Lozinka"];
                    bool Active = (bool)row["Active"];

                    TerminDAO terminDao = new TerminDAO();
                    if (row["TipKorisnika"].Equals(TipKorisnika.Administrator.ToString()))
                    {
                        Administrator admin = new Administrator(Ime, Prezime, Email, KorisnickoIme, Lozinka, Active);
                        korisnici.Add(admin);
                    }
                    if (row["TipKorisnika"].Equals(TipKorisnika.Profesor.ToString()))
                    {
                        Profesor profesor = new Profesor(Ime, Prezime, Email, KorisnickoIme, Lozinka, Active);
                        profesor.Ustanova = UstanovaDao.GetUstanovabyUser(KorisnickoIme);
                        korisnici.Add(profesor);
                        List<Termin> termini = terminDao.SelectTermini(profesor);
                    }
                    if (row["TipKorisnika"].Equals(TipKorisnika.Asistent.ToString()))
                    {
                        string profesorKorisnickoIme = GetProf(KorisnickoIme);
                        Profesor profesor = new Profesor();
                        Asistent asistent = new Asistent(Ime, Prezime, Email, KorisnickoIme, Lozinka, profesor, Active);
                        asistent.Ustanova = UstanovaDao.GetUstanovabyUser(KorisnickoIme);
                        korisnici.Add(asistent);
                        List<Termin> termini = terminDao.SelectTermini(asistent);
                    }
                    
                }
            }

            return korisnici;
        }

        public string CreateKorisnik(Korisnik korisnik)
        {
            string id = "";
            using (SqlConnection conn = new SqlConnection(Util.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Korisnici (KorisnickoIme, Ime, Prezime, Email, Lozinka, TipKorisnika, SifraUstanove, KImeProfesor, Active)
                     VALUES (@KorisnickoIme, @Ime, @Prezime, @Email, @Lozinka, @TipKorisnika, @SifraUstanove, @KImeProfesor, @Active)";

                command.Parameters.Add(new SqlParameter("KorisnickoIme", korisnik.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Ime", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", korisnik.Prezime));
                command.Parameters.Add(new SqlParameter("Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", korisnik.Lozinka));
                command.Parameters.Add(new SqlParameter("TipKorisnika", korisnik.TipKorisnika.ToString()));
                if (korisnik.Ustanova == null)
                {
                    command.Parameters.Add(new SqlParameter("SifraUstanove", ""));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("SifraUstanove", korisnik.Ustanova.sifra));
                }
                command.Parameters.Add(new SqlParameter("KImeProfesor", ""));
                command.Parameters.Add(new SqlParameter("Active", korisnik.Active));


                id = (string)command.ExecuteScalar();
                return id;
            }

        }

        public virtual void UpdateKorisnika(Korisnik korisnik)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Korisnici SET Ime=@Ime, Prezime=@Prezime, Email=@Email, Lozinka=@Lozinka where KorisnickoIme=@KorisnickoIme";

                command.Parameters.Add(new SqlParameter("Ime", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", korisnik.Prezime));
                command.Parameters.Add(new SqlParameter("Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", korisnik.Lozinka));
                command.Parameters.Add(new SqlParameter("KorisnickoIme", korisnik.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateAsistenti(Asistent asistent, Profesor profesor)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Korisnici SET KImeProfesor=@KImeProfesor where KImeProfesor=@KorisnickoImeProfesora";

                command.Parameters.Add(new SqlParameter("KImeProfesor", ""));
                command.Parameters.Add(new SqlParameter("KorisnickoImeProfesora", profesor.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateAsistent(Asistent asistent)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Korisnici SET KImeProfesor=@KImeProfesor WHERE KorisnickoIme=@KorisnickoIme";

                command.Parameters.Add(new SqlParameter("KImeProfesor", asistent.Profesor.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("KorisnickoIme", asistent.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public void DeleteKorisnik(Korisnik korisnik)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Korisnici SET Active=@Active where KorisnickoIme=@KorisnickoIme";

                command.Parameters.Add(new SqlParameter("Active", korisnik.Active = false));
                command.Parameters.Add(new SqlParameter("KorisnickoIme", korisnik.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }


        public void DeleteProfesor(Profesor profesor)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Korisnici SET Active=@Active where KorisnickoIme=@KorisnickoIme";

                command.Parameters.Add(new SqlParameter("Active", profesor.Active = false));
                command.Parameters.Add(new SqlParameter("KorisnickoIme", profesor.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public void UkloniAsistenta(Asistent asistent)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Korisnici SET KImeProfesor=@KImeProfesor where KorisnickoIme=@KorisnickoIme";

                command.Parameters.Add(new SqlParameter("KImeProfesor", ""));
                command.Parameters.Add(new SqlParameter("KorisnickoIme", asistent.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public string CreateProfesor(Profesor profesor)
        {
            string id = "";
            using (SqlConnection conn = new SqlConnection(Util.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Korisnici (KorisnickoIme, Ime, Prezime, Email, Lozinka, TipKorisnika, SifraUstanove, KImeProfesor, Active)
                     VALUES (@KorisnickoIme, @Ime, @Prezime, @Email, @Lozinka, @TipKorisnika, @SifraUstanove, @KImeProfesor, @Active)";

                command.Parameters.Add(new SqlParameter("KorisnickoIme", profesor.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Ime", profesor.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", profesor.Prezime));
                command.Parameters.Add(new SqlParameter("Email", profesor.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", profesor.Lozinka));
                command.Parameters.Add(new SqlParameter("TipKorisnika", profesor.TipKorisnika.ToString()));
                if (profesor.Ustanova == null)
                {
                    command.Parameters.Add(new SqlParameter("SifraUstanove", ""));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("SifraUstanove", profesor.Ustanova.sifra));
                }
                command.Parameters.Add(new SqlParameter("KImeProfesor", ""));
                command.Parameters.Add(new SqlParameter("Active", profesor.Active));

                id = (string)command.ExecuteScalar();
                return id;
                
            }

        }

        public string CreateAsistent(Asistent asistent)
        {
            string id = "";
            using (SqlConnection conn = new SqlConnection(Util.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Korisnici (KorisnickoIme, Ime, Prezime, Email, Lozinka, TipKorisnika, SifraUstanove, KImeProfesor, Active)
                     VALUES (@KorisnickoIme, @Ime, @Prezime, @Email, @Lozinka, @TipKorisnika, @SifraUstanove, @KImeProfesor, @Active)";

                command.Parameters.Add(new SqlParameter("KorisnickoIme", asistent.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Ime", asistent.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", asistent.Prezime));
                command.Parameters.Add(new SqlParameter("Email", asistent.Email));
                command.Parameters.Add(new SqlParameter("Lozinka", asistent.Lozinka));
                command.Parameters.Add(new SqlParameter("TipKorisnika", asistent.TipKorisnika.ToString()));
                if (asistent.Ustanova == null)
                {
                    command.Parameters.Add(new SqlParameter("SifraUstanove", ""));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("SifraUstanove", asistent.Ustanova.sifra));
                }
                if (asistent.Profesor == null)
                {
                    command.Parameters.Add(new SqlParameter("KImeProfesor", ""));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("KImeProfesor", asistent.Profesor.KorisnickoIme));
                }
                command.Parameters.Add(new SqlParameter("Active", asistent.Active));


                id = (string)command.ExecuteScalar();
                return id;
            }
        }

        public static void PretragaKorisnika(string s, string parametar, List<Korisnik> korisnici)
        {
            korisnici.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Korisnicko ime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE KorisnickoIme LIKE @s AND Active=1";
                }
                if (parametar.Equals("Ime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Ime LIKE @s AND Active=1";
                }
                if (parametar.Equals("Prezime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Prezime LIKE @s AND Active=1";
                }
                if (parametar.Equals("Email"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Email LIKE @s AND Active=1";
                }
                if (parametar.Equals("Tip korisnika"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE TipKorisnika LIKE @s AND Active=1";
                }
                if (parametar.Equals(""))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Active=1";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");

                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string Ime = (string)row["Ime"];
                    string Prezime = (string)row["Prezime"];
                    string Email = (string)row["Email"];
                    string KorisnickoIme = (string)row["KorisnickoIme"];
                    string Lozinka = (string)row["Lozinka"];
                    bool Active = (bool)row["Active"];

                    TerminDAO terminDao = new TerminDAO();
                    if (row["TipKorisnika"].Equals(TipKorisnika.Administrator.ToString()))
                    {
                        Administrator admin = new Administrator(Ime, Prezime, Email, KorisnickoIme, Lozinka, Active);
                        korisnici.Add(admin);
                    }
                    if (row["TipKorisnika"].Equals(TipKorisnika.Profesor.ToString()))
                    {
                        Profesor profesor = new Profesor(Ime, Prezime, Email, KorisnickoIme, Lozinka, Active);
                        profesor.Ustanova = UstanovaDao.GetUstanovabyUser(KorisnickoIme);
                        korisnici.Add(profesor);
                        List<Termin> termini = terminDao.SelectTermini(profesor);
                    }
                    if (row["TipKorisnika"].Equals(TipKorisnika.Asistent.ToString()))
                    {
                        string profesorKorisnickoIme = GetProf(KorisnickoIme);
                        Profesor profesor = new Profesor();
                        Asistent asistent = new Asistent(Ime, Prezime, Email, KorisnickoIme, Lozinka, profesor, Active);
                        asistent.Ustanova = UstanovaDao.GetUstanovabyUser(KorisnickoIme);
                        korisnici.Add(asistent);
                        List<Termin> termini = terminDao.SelectTermini(asistent);
                    }
                }

            }
        }

        public static void PretragaZaposlenih(string s, string parametar, List<Korisnik> zaposleni, Ustanova ustanova)
        {
            zaposleni.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                if (parametar.Equals("Ime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Ime LIKE @s AND Active=1 AND SifraUstanove=@SifraUstanove";
                }
                if (parametar.Equals("Prezime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Prezime LIKE @s AND Active=1 AND SifraUstanove=@SifraUstanove";
                }
                if (parametar.Equals("Email"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Email LIKE @s AND Active=1 AND SifraUstanove=@SifraUstanove";
                }
                if (parametar.Equals("Tip korisnika"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE TipKorisnika LIKE @s AND Active=1 AND SifraUstanove=@SifraUstanove";
                }
                if (parametar.Equals(""))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Active=1 AND SifraUstanove=@SifraUstanove";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                command.Parameters.Add(new SqlParameter("SifraUstanove", ustanova.sifra));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");

                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string Ime = (string)row["Ime"];
                    string Prezime = (string)row["Prezime"];
                    string Email = (string)row["Email"];
                    string KorisnickoIme = (string)row["KorisnickoIme"];
                    string Lozinka = (string)row["Lozinka"];
                    bool Active = (bool)row["Active"];

                    TerminDAO terminDao = new TerminDAO();
                    if (row["TipKorisnika"].Equals(TipKorisnika.Profesor.ToString()))
                    {
                        Profesor profesor = new Profesor(Ime, Prezime, Email, KorisnickoIme, Lozinka, Active);
                        profesor.Ustanova = UstanovaDao.GetUstanovabyUser(KorisnickoIme);
                        zaposleni.Add(profesor);
                        List<Termin> termini = terminDao.SelectTermini(profesor);
                    }
                    if (row["TipKorisnika"].Equals(TipKorisnika.Asistent.ToString()))
                    {
                        string profesorKorisnickoIme = GetProf(KorisnickoIme);
                        Profesor profesor = new Profesor();
                        Asistent asistent = new Asistent(Ime, Prezime, Email, KorisnickoIme, Lozinka, profesor, Active);
                        asistent.Ustanova = UstanovaDao.GetUstanovabyUser(KorisnickoIme);
                        zaposleni.Add(asistent);
                        List<Termin> termini = terminDao.SelectTermini(asistent);
                    }
                }

            }
        }


        public static void PretragaAsistenata(string s, string parametar, Profesor profesor)
        {
            profesor.Asistenti.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Korisnicko ime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE KorisnickoIme LIKE @s AND Active=1 AND KImeProfesor=@KImeProfesor";
                }
                if (parametar.Equals("Ime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Ime LIKE @s AND Active=1 AND KImeProfesor=@KImeProfesor";
                }
                if (parametar.Equals("Prezime"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Prezime LIKE @s AND Active=1 AND KImeProfesor=@KImeProfesor";
                }
                if (parametar.Equals("Email"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Email LIKE @s AND Active=1 AND KImeProfesor=@KImeProfesor";
                }
                if (parametar.Equals("Tip korisnika"))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE TipKorisnika LIKE @s AND Active=1 AND KImeProfesor=@KImeProfesor";
                }
                if (parametar.Equals(""))
                {
                    command.CommandText = @"SELECT * FROM Korisnici WHERE Active=1 AND KImeProfesor=@KImeProfesor";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                command.Parameters.Add(new SqlParameter("KImeProfesor", profesor.KorisnickoIme));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");

                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string Ime = (string)row["Ime"];
                    string Prezime = (string)row["Prezime"];
                    string Email = (string)row["Email"];
                    string KorisnickoIme = (string)row["KorisnickoIme"];
                    string Lozinka = (string)row["Lozinka"];
                    bool Active = (bool)row["Active"];

                    Asistent asistent = new Asistent(Ime, Prezime, Email, KorisnickoIme, Lozinka, profesor, Active);
                    asistent.Ustanova = UstanovaDao.GetUstanovabyUser(KorisnickoIme);
                    profesor.Asistenti.Add(asistent);
                    TerminDAO terminDao = new TerminDAO();
                    List<Termin> termini = terminDao.SelectTermini(asistent);
                }

            }
        }

        /*        public List<Asistent> SelectAsistenti()
                {
                    List<Asistent> asistenti = new List<Asistent>();

                    using (SqlConnection connection = new SqlConnection(Util.ConnectionString))
                    {
                        connection.Open();

                        DataSet ds = new DataSet();

                        SqlCommand command = connection.CreateCommand();
                        command.CommandText = "SELECT * FROM Korisnici WHERE TipKorisnika=@TipKorisnika AND Active=1";
                        command.Parameters.Add(new SqlParameter("TipKorisnika", TipKorisnika.Asistent));

                        SqlDataAdapter dataAdapter = new SqlDataAdapter();
                        dataAdapter.SelectCommand = command;
                        dataAdapter.Fill(ds, "Korisnici");

                        //                var dataTable = ds.Tables["Ustanova"].AsEnumerable();
                        //                dataTable.Where(row => ((string)row["Naziv"]).Contains("FTN")).Count();

                        foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                        {
                            Asistent asistent = new Asistent();
                            asistent.Ime = (string)row["Ime"];
                            asistent.Prezime = (string)row["Prezime"];
                            asistent.Email = (string)row["Email"];
                            asistent.KorisnickoIme = (string)row["KorisnickoIme"];
                            asistent.Lozinka = (string)row["Lozinka"];
                            asistent.TipKorisnika = (TipKorisnika)Enum.Parse(typeof(TipKorisnika), (string)row["TipKorisnika"]);
                            asistent.ustanova.sifra = (int)row["SifraUstanove"];
                            asistent.profesor.KorisnickoIme = (string)row["KImeProfesor"];
                            asistent.Active = (bool)row["Active"];

                            asistenti.Add(asistent);
                        }
                    }

                    return asistenti;
                }

                public List<Asistent> SelectAsistenti(Profesor profesor)
                {
                    profesor.Asistenti.Clear();
                    List<Asistent> asistenti = new List<Asistent>();

                    using (SqlConnection connection = new SqlConnection(Util.ConnectionString))
                    {
                        connection.Open();

                        DataSet ds = new DataSet();

                        SqlCommand command = connection.CreateCommand();
                        command.CommandText = "SELECT * FROM Korisnici WHERE TipKorisnika=@TipKorisnika Active=1 AND KImeProfesor=@KImeProfesor";
                        command.Parameters.Add(new SqlParameter("TipKorisnika", TipKorisnika.Asistent));
                        command.Parameters.Add(new SqlParameter("KImeProfesor", profesor.KorisnickoIme));

                        SqlDataAdapter dataAdapter = new SqlDataAdapter();
                        dataAdapter.SelectCommand = command;
                        dataAdapter.Fill(ds, "Asistenti");

                        //                var dataTable = ds.Tables["Ustanova"].AsEnumerable();
                        //                dataTable.Where(row => ((string)row["Naziv"]).Contains("FTN")).Count();

                        foreach (DataRow row in ds.Tables["Asistenti"].Rows)
                        {
                            Asistent asistent = new Asistent();
                            asistent.Ime = (string)row["Ime"];
                            asistent.Prezime = (string)row["Prezime"];
                            asistent.Email = (string)row["Email"];
                            asistent.KorisnickoIme = (string)row["KorisnickoIme"];
                            asistent.Lozinka = (string)row["Lozinka"];
                            asistent.TipKorisnika = (TipKorisnika)Enum.Parse(typeof(TipKorisnika), (string)row["TipKorisnika"]);
                            asistent.profesor.KorisnickoIme = (string)row["KImeProfesor"];
                            asistent.Active = (bool)row["Active"];

                            asistenti.Add(asistent);
                        }
                    }

                    return asistenti;
                }*/

        public static string GetProf(String KIme)
        {
            using (SqlConnection connection = new SqlConnection(Util.ConnectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"SELECT KorisnickoIme
                                        from Korisnici
                                        where KorisnickoIme=(select KImeProfesor
				                                    from Korisnici
				                                    where KorisnickoIme=(select KorisnickoIme
							                                    from Korisnici
							                                    where KorisnickoIme=@KorisnickoIme and Active=1))";
                command.Parameters.Add(new SqlParameter("KorisnickoIme", KIme));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");
                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string KorisnickoIme = (string)row["KorisnickoIme"];
                    return KorisnickoIme;
                }
            }
            return null;
        }
    }
}
