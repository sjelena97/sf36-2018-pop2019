﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe;

namespace Projekat.DAO
{
    public class UstanovaDao
    {
        public List<Ustanova> SelectUstanova()
        {
            List<Ustanova> ustanove = new List<Ustanova>();

            using(SqlConnection connection = new SqlConnection(Util.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Ustanova WHERE Active=1";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ustanova");

//                var dataTable = ds.Tables["Ustanova"].AsEnumerable();
//                dataTable.Where(row => ((string)row["Naziv"]).Contains("FTN")).Count();

                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    Ustanova ustanova = new Ustanova();
                    ustanova.sifra = (int)row["sifra"];
                    ustanova.naziv = (string)row["naziv"];
                    ustanova.lokacija = (string)row["lokacija"];
                    ustanova.Active = (bool)row["Active"];

                    UcionicaDAO ucionicaDAO = new UcionicaDAO();
                    List<Ucionica> ucionice = ucionicaDAO.SelectUcionica(ustanova);
                    ustanove.Add(ustanova);
                }
            }

            return ustanove;
        }
        public int CreateUstanova(Ustanova ustanova)
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Util.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Ustanova (Naziv, Lokacija, Active)
                    output inserted.sifra VALUES (@Naziv, @Lokacija, @Active)";

                command.Parameters.Add(new SqlParameter("Naziv", ustanova.naziv));
                command.Parameters.Add(new SqlParameter("Lokacija", ustanova.lokacija));
                command.Parameters.Add(new SqlParameter("Active", ustanova.Active));

                id= (int)command.ExecuteScalar();
                return id;
            }

        }

        public void UpdateUstanova(Ustanova ustanova)
        {
            using(SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                    UPDATE Ustanova 
                    SET Naziv =@Naziv, Lokacija=@Lokacija, Active=@Active
                    WHERE Sifra=@Sifra
                ";

                command.Parameters.Add(new SqlParameter("Naziv", ustanova.naziv));
                command.Parameters.Add(new SqlParameter("Lokacija", ustanova.lokacija));
                command.Parameters.Add(new SqlParameter("Active", ustanova.Active));
                command.Parameters.Add(new SqlParameter("Sifra", ustanova.sifra));

                command.ExecuteNonQuery();
            }
        }

        public void DeleteUstanova(Ustanova ustanova)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Ustanova SET Active=@Active where Sifra=@Sifra";

                command.Parameters.Add(new SqlParameter("Active", ustanova.Active = false));
                command.Parameters.Add(new SqlParameter("Sifra", ustanova.sifra));

                command.ExecuteNonQuery();
            }
        }

        public static int SifraZaUstanovu()
        {
            int max = 0;
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"SELECT max(Sifra) as MAX
                                        FROM Ustanova";

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanova");
                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    max = (int)row["MAX"];
                }
            }
            return max + 1;
        }

        public static Ustanova GetUstanovabyUser(String KIme)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"SELECT *
                                        from Ustanova
                                        where Sifra=(select SifraUstanove					                                          
								                                from Korisnici
								                                where KorisnickoIme=@KorisnickoIme and Active=1)";
                command.Parameters.Add(new SqlParameter("KorisnickoIme", KIme));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanova");
                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    Ustanova ustanova = new Ustanova();
                    ustanova.sifra = (int)row["Sifra"];
                    ustanova.naziv = (string)row["Naziv"];
                    ustanova.lokacija = (string)row["Lokacija"];
                    ustanova.Active = (bool)row["Active"];

                    UcionicaDAO ucionicaDAO = new UcionicaDAO();
                    List<Ucionica> ucionice = ucionicaDAO.SelectUcionica(ustanova);
                    return ustanova;
                }
            }

            return null;
        }

        public static void PretragaUstanova(string s, string parametar, List<Ustanova> ustanove)
        {
            ustanove.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Naziv"))
                {
                    command.CommandText = @"SELECT * FROM Ustanova WHERE Naziv LIKE @s AND Active=1";
                }
                if (parametar.Equals("Lokacija"))
                {
                    command.CommandText = @"SELECT * FROM Ustanova WHERE Lokacija LIKE @s AND Active=1";
                }
                if (parametar.Equals(""))
                {
                    command.CommandText = @"SELECT * FROM Ustanova WHERE Active=1";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanova");

                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    int sifra = (int)row["Sifra"];
                    string naziv = (string)row["Naziv"];
                    string lokacija = (string)row["Lokacija"];
                    bool active = (bool)row["Active"];

                    Ustanova ustanova = new Ustanova(sifra, naziv, lokacija, active);
                    ustanove.Add(ustanova);
                }

            }
        }
    }
}
