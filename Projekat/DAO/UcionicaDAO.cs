﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe;

namespace Projekat.DAO
{
    public class UcionicaDAO
    {
        List<Ucionica> ucionice = new List<Ucionica>();
        public List<Ucionica> SelectUcionica(Ustanova ustanova)
        {
            ustanova.Ucionice.Clear();
            using (SqlConnection connection = new SqlConnection(Util.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Ucionice WHERE SifraUstanove=@SifraUstanove";
                command.Parameters.Add(new SqlParameter("SifraUstanove", ustanova.sifra));

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ucionice");

/*                var dataTable = ds.Tables["Ucionica"].AsEnumerable();
                dataTable.Where(row => ((string)row["TipUcionice"]).Contains("Racunarska")).Count();*/

                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    Ucionica ucionica = new Ucionica();
                    ucionica.Sifra = (int)row["Sifra"];
                    ucionica.BrojUcionice = (string)row["BrojUcionice"];
                    ucionica.BrojMesta = (int)row["BrojMesta"];
                    ucionica.TipUcionice = (TipUcionice)Enum.Parse(typeof(TipUcionice), (string)row["TipUcionice"]);
                    ucionica.ustanova.sifra = (int)row["SifraUstanove"];
                    ucionica.Active = (bool)row["Active"];

                    ustanova.Ucionice.Add(ucionica);
                    TerminDAO terminDao = new TerminDAO();
                    List<Termin> termini = terminDao.SelectTermini(ucionica);
                }
            }

            return ucionice;
        }

        public int CreateUcionica(Ucionica ucionica, Ustanova ustanova)
        {
            int sifra = -1;
            using (SqlConnection conn = new SqlConnection(Util.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Ucionice (BrojUcionice, BrojMesta, TipUcionice, SifraUstanove, Active)
                    output inserted.sifra VALUES (@BrojUcionice, @BrojMesta, @TipUcionice, @SifraUstanove, @Active)";

                command.Parameters.Add(new SqlParameter("BrojUcionice", ucionica.BrojUcionice));
                command.Parameters.Add(new SqlParameter("BrojMesta", ucionica.BrojMesta));
                command.Parameters.Add(new SqlParameter("TipUcionice", ucionica.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("SifraUstanove", ustanova.sifra));
                command.Parameters.Add(new SqlParameter("Active", ucionica.Active));

                sifra = (int)command.ExecuteScalar();
                return sifra;
            }

        }

        public void UpdateUcionica(Ucionica ucionica)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"
                    UPDATE Ucionice 
                    SET BrojUcionice =@BrojUcionice, BrojMesta=@BrojMesta, TipUcionice=@TipUcionice, Active=@Active
                    WHERE Sifra=@Sifra
                ";

                command.Parameters.Add(new SqlParameter("BrojUcionice", ucionica.BrojUcionice));
                command.Parameters.Add(new SqlParameter("BrojMesta", ucionica.BrojMesta));
                command.Parameters.Add(new SqlParameter("TipUcionice", ucionica.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("Active", ucionica.Active));
                command.Parameters.Add(new SqlParameter("Sifra", ucionica.Sifra));


                command.ExecuteNonQuery();
            }
        }

        public void DeleteUcionica(Ucionica ucionica)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Ucionice SET Active=@Active where Sifra=@Sifra";

                command.Parameters.Add(new SqlParameter("Active", ucionica.Active = false));
                command.Parameters.Add(new SqlParameter("Sifra", ucionica.Sifra));

                command.ExecuteNonQuery();
            }
        }

        public void DeleteUcionica(Ucionica ucionica, Ustanova ustanova)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Ucionice SET Active=@Active where SifraUstanove=@SifraUstanove";

                command.Parameters.Add(new SqlParameter("Active", ucionica.Active = false));
                command.Parameters.Add(new SqlParameter("SifraUstanove", ustanova.sifra));

                command.ExecuteNonQuery();
            }
        }

        public static void PretragaUcionica(string s, string parametar, Ustanova ustanova)
        {
            ustanova.Ucionice.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Broj ucionice"))
                {
                    command.CommandText = @"SELECT * FROM Ucionice WHERE BrojUcionice LIKE @s AND SifraUstanove=@SifraUstanove AND Active=1";
                }
                if (parametar.Equals("Broj mesta"))
                {
                    command.CommandText = @"SELECT * FROM Ucionice WHERE BrojMesta LIKE @s AND SifraUstanove=@SifraUstanove AND Active=1";
                }
                if(parametar.Equals("Tip ucionice"))
                {
                    command.CommandText = @"SELECT * FROM Ucionice WHERE TipUcionice LIKE @s AND SifraUstanove=@SifraUstanove AND Active=1";
                }
                if (parametar.Equals(""))
                {
                    command.CommandText = @"SELECT * FROM Ucionice WHERE Active=1 AND SifraUstanove=@SifraUstanove";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                command.Parameters.Add(new SqlParameter("SifraUstanove", ustanova.sifra));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionice");

                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    int sifra = (int)row["Sifra"];
                    string brojUcionice = (string)row["BrojUcionice"];
                    int brojMesta = (int)row["BrojMesta"];
                    TipUcionice tipUcionice= (TipUcionice)Enum.Parse(typeof(TipUcionice), (string)row["TipUcionice"]);
                    bool active = (bool)row["Active"];

                    Ucionica ucionica = new Ucionica(sifra, brojUcionice, brojMesta, tipUcionice, ustanova, active);
                    ustanova.Ucionice.Add(ucionica);
                }

            }
        }

        public static int SifraZaUcionicu(Ustanova ustanova)
        {
            int max = 0;
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"SELECT max(Sifra) as MAX
                                        FROM Ucionice WHERE SifraUstanove=@SifraUstanove";

                command.Parameters.Add(new SqlParameter("SifraUstanove", ustanova.sifra));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionice");
                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    try
                    {
                        max = (int)row["MAX"];
                    }
                    catch
                    {
                        
                    }
                }
            }
            return max + 1;
        }
    }
}
