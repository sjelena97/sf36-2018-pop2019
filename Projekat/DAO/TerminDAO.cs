﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vezbe;
using Vezbe.Model;

namespace Projekat.DAO
{
    public class TerminDAO
    {
            List<Termin> termini = new List<Termin>();

            public List<Termin> SelectTermini(Ucionica ucionica)
            {
                using (SqlConnection connection = new SqlConnection(Util.ConnectionString))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT * FROM Termini WHERE Active=1 AND SifraUcionice=@SifraUcionice";
                    command.Parameters.Add(new SqlParameter("SifraUcionice", ucionica.Sifra));

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(ds, "Termini");

                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {

                    Termin termin = new Termin();
                    termin.Sifra = (int)row["Sifra"];
                    termin.TipNastave = (TipNastave)Enum.Parse(typeof(TipNastave), (string)row["TipNastave"]);
                    termin.Dan = (Dani)Enum.Parse(typeof(Dani), (string)row["Dan"]);
                    termin.TerminOd = (DateTime)row["TerminOd"];
                    termin.TerminDo = (DateTime)row["TerminDo"];
                    termin.Ucionica.Sifra = (int)row["SifraUcionice"];
                    termin.Korisnik.KorisnickoIme = (string)row["KorisnickoImeKorisnika"];
                    termin.Active = (bool)row["Active"];

                    ucionica.termini.Add(termin);
                }
                }

                return termini;
            }

        public List<Termin> SelectTermini(Korisnik korisnik){
            korisnik.Termini.Clear();

            using (SqlConnection connection = new SqlConnection(Util.ConnectionString))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Termini WHERE Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                command.Parameters.Add(new SqlParameter("KorisnickoImeKorisnika", korisnik.KorisnickoIme));

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Termini");

                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {

                    Termin termin = new Termin();
                    termin.Sifra = (int)row["Sifra"];
                    termin.TipNastave = (TipNastave)Enum.Parse(typeof(TipNastave), (string)row["TipNastave"]);
                    termin.Dan = (Dani)Enum.Parse(typeof(Dani), (string)row["Dan"]);
                    termin.TerminOd = (DateTime)row["TerminOd"];
                    termin.TerminDo = (DateTime)row["TerminDo"];
                    termin.Ucionica.Sifra = (int)row["SifraUcionice"];
                    termin.Korisnik.KorisnickoIme = (string)row["KorisnickoImeKorisnika"];
                    termin.Active = (bool)row["Active"];

                    korisnik.Termini.Add(termin);
                }
            }

            return termini;
        }


        public static void PretragaTermina(string s, string parametar, Ucionica ucionica)
        {
            ucionica.termini.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Tip nastave"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE TipNastave LIKE @s AND SifraUcionice=@SifraUcionice AND Active=1";
                }
                if (parametar.Equals("Dan"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @s AND SifraUcionice=@SifraUcionice AND Active=1";
                }
                if (parametar.Equals("Termin od"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE TerminOd LIKE @s AND SifraUcionice=@SifraUcionice AND Active=1";
                }
                if (parametar.Equals("Termin do"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE TerminDo LIKE @s AND SifraUcionice=@SifraUcionice AND Active=1";
                }
                if (parametar.Equals("Korisnik"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE KorisnickoImeKorisnika LIKE @s AND SifraUcionice=@SifraUcionice AND Active=1";
                }
                if (parametar.Equals(""))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Active=1 AND SifraUcionice=@SifraUcionice";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                command.Parameters.Add(new SqlParameter("SifraUcionice", ucionica.Sifra));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");

                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    Termin termin = new Termin();
                    termin.Sifra = (int)row["Sifra"];
                    termin.TipNastave = (TipNastave)Enum.Parse(typeof(TipNastave), (string)row["TipNastave"]);
                    termin.Dan = (Dani)Enum.Parse(typeof(Dani), (string)row["Dan"]);
                    termin.TerminOd = (DateTime)row["TerminOd"];
                    termin.TerminDo = (DateTime)row["TerminDo"];
                    termin.Ucionica.Sifra = (int)row["SifraUcionice"];
                    termin.Korisnik.KorisnickoIme = (string)row["KorisnickoImeKorisnika"];
                    termin.Active = (bool)row["Active"];

                    ucionica.termini.Add(termin);
                }

            }
        }

        public static void PretragaTermina(string s, string parametar, Korisnik korisnik)
        {
            korisnik.Termini.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Tip nastave"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE TipNastave LIKE @s AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika AND Active=1";
                }
                if (parametar.Equals("Dan"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @s AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika AND Active=1";
                }
                if (parametar.Equals("Termin od"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE TerminOd LIKE @s AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika AND Active=1";
                }
                if (parametar.Equals("Termin do"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE TerminDo LIKE @s AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika AND Active=1";
                }
                if (parametar.Equals("Korisnik"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE KorisnickoImeKorisnika LIKE @s AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika AND Active=1";
                }
                if (parametar.Equals(""))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                command.Parameters.Add(new SqlParameter("KorisnickoImeKorisnika", korisnik.KorisnickoIme));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");

                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    Termin termin = new Termin();
                    termin.Sifra = (int)row["Sifra"];
                    termin.TipNastave = (TipNastave)Enum.Parse(typeof(TipNastave), (string)row["TipNastave"]);
                    termin.Dan = (Dani)Enum.Parse(typeof(Dani), (string)row["Dan"]);
                    termin.TerminOd = (DateTime)row["TerminOd"];
                    termin.TerminDo = (DateTime)row["TerminDo"];
                    termin.Ucionica.Sifra = (int)row["SifraUcionice"];
                    termin.Korisnik.KorisnickoIme = (string)row["KorisnickoImeKorisnika"];
                    termin.Active = (bool)row["Active"];

                    korisnik.Termini.Add(termin);
                }

            }
        }

        public void DeleteTermin(Termin termin)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Termini SET Active=@Active where Sifra=@Sifra";

                command.Parameters.Add(new SqlParameter("Active", termin.Active = false));
                command.Parameters.Add(new SqlParameter("Sifra", termin.Sifra));

                command.ExecuteNonQuery();
            }
        }

        public void DeleteTermini(Termin termin, Ucionica ucionica)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandText = @"UPDATE Termini SET Active=@Active where SifraUcionice=@SifraUcionice";

                command.Parameters.Add(new SqlParameter("Active", termin.Active = false));
                command.Parameters.Add(new SqlParameter("SifraUstanove", ucionica.Sifra));

                command.ExecuteNonQuery();
            }
        }

        public static void PretragaTerminaZaRaspored(string parametar, ObservableCollection<Termin> termini, Ustanova ustanova)
        {
            termini.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Ponedjeljak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND SifraUcionice in(SELECT Sifra FROM Ucionice WHERE SifraUstanove=@SifraUstanove)";
                }
                if (parametar.Equals("Utorak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND SifraUcionice in(SELECT Sifra FROM Ucionice WHERE SifraUstanove=@SifraUstanove)";
                }
                if (parametar.Equals("Srijeda"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND SifraUcionice in(SELECT Sifra FROM Ucionice WHERE SifraUstanove=@SifraUstanove)";
                }
                if (parametar.Equals("Cetvrtak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND SifraUcionice in(SELECT Sifra FROM Ucionice WHERE SifraUstanove=@SifraUstanove)";
                }
                if (parametar.Equals("Petak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND SifraUcionice in(SELECT Sifra FROM Ucionice WHERE SifraUstanove=@SifraUstanove)";
                }
                if (parametar.Equals("Subota"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND SifraUcionice in(SELECT Sifra FROM Ucionice WHERE SifraUstanove=@SifraUstanove)";
                }
                if (parametar.Equals("Nedjelja"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND SifraUcionice in(SELECT Sifra FROM Ucionice WHERE SifraUstanove=@SifraUstanove)";
                }
                command.Parameters.Add(new SqlParameter("parametar",  parametar));
                command.Parameters.Add(new SqlParameter("SifraUstanove", ustanova.sifra));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");

                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    Termin termin = new Termin();
                    termin.Sifra = (int)row["Sifra"];
                    termin.TipNastave = (TipNastave)Enum.Parse(typeof(TipNastave), (string)row["TipNastave"]);
                    termin.Dan = (Dani)Enum.Parse(typeof(Dani), (string)row["Dan"]);
                    termin.TerminOd = (DateTime)row["TerminOd"];
                    termin.TerminDo = (DateTime)row["TerminDo"];
                    termin.Ucionica.Sifra = (int)row["SifraUcionice"];
                    termin.Korisnik.KorisnickoIme = (string)row["KorisnickoImeKorisnika"];
                    termin.Active = (bool)row["Active"];

                    termini.Add(termin);
                }

            }
        }

        public int CreateTermin(Termin termin, Ucionica ucionica)
        {
            int sifra = -1;
            using (SqlConnection conn = new SqlConnection(Util.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    INSERT INTO Termini (TipNastave, Dan, TerminOd, TerminDo, SifraUcionice, KorisnickoImeKorisnika, Active)
                    output inserted.Sifra VALUES (@TipNastave, @Dan, @TerminOd, @TerminDo, @SifraUcionice, @KorisnickoImeKorisnika, @Active)";

                command.Parameters.Add(new SqlParameter("TipNastave", termin.TipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("Dan", termin.Dan.ToString()));
                command.Parameters.Add(new SqlParameter("TerminOd", termin.TerminOd));
                command.Parameters.Add(new SqlParameter("TerminDo", termin.TerminDo));
                command.Parameters.Add(new SqlParameter("SifraUcionice", ucionica.Sifra));
                command.Parameters.Add(new SqlParameter("KorisnickoImeKorisnika", termin.Korisnik.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Active", termin.Active));

                sifra = (int)command.ExecuteScalar();
                return sifra;
            }

        }

        public static void PretragaTerminaZaRasporedKorisnika(string parametar, List<Termin> termini, Korisnik korisnik)
        {
            termini.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Util.ConnectionString))
            {
                sqlConnection.Open();
                SqlCommand command = sqlConnection.CreateCommand();

                if (parametar.Equals("Ponedjeljak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                if (parametar.Equals("Utorak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                if (parametar.Equals("Srijeda"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                if (parametar.Equals("Cetvrtak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                if (parametar.Equals("Petak"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                if (parametar.Equals("Subota"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                if (parametar.Equals("Nedjelja"))
                {
                    command.CommandText = @"SELECT * FROM Termini WHERE Dan LIKE @parametar AND Active=1 AND KorisnickoImeKorisnika=@KorisnickoImeKorisnika";
                }
                command.Parameters.Add(new SqlParameter("parametar", parametar));
                command.Parameters.Add(new SqlParameter("KorisnickoImeKorisnika", korisnik.KorisnickoIme));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");

                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    Termin termin = new Termin();
                    termin.Sifra = (int)row["Sifra"];
                    termin.TipNastave = (TipNastave)Enum.Parse(typeof(TipNastave), (string)row["TipNastave"]);
                    termin.Dan = (Dani)Enum.Parse(typeof(Dani), (string)row["Dan"]);
                    termin.TerminOd = (DateTime)row["TerminOd"];
                    termin.TerminDo = (DateTime)row["TerminDo"];
                    termin.Ucionica.Sifra = (int)row["SifraUcionice"];
                    termin.Korisnik.KorisnickoIme = (string)row["KorisnickoImeKorisnika"];
                    termin.Active = (bool)row["Active"];

                    termini.Add(termin);
                }

            }
        }
    }
}
