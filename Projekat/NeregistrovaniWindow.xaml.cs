﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for NeregistrovaniWindow.xaml
    /// </summary>
    public partial class NeregistrovaniWindow : Window
    {
        public NeregistrovaniWindow()
        {
            InitializeComponent();
            Korisnik korisnik = new Korisnik();
            Util.prijavljeniKorisnik = korisnik;
        }

        private void UstanoveBtn_Click(object sender, RoutedEventArgs e)
        {
            UstanoveWindow WindowUstanove = new UstanoveWindow();
            WindowUstanove.Show();
        }
    }
}
