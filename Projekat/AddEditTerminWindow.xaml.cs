﻿using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for AddEditTerminWindow.xaml
    /// </summary>
    public partial class AddEditTerminWindow : Window
    {
        public Termin Termin;

        public enum UserWindowState { Addition, Editing }
        private UserWindowState state;
        public Ustanova Ustanova;
        public Ucionica Ucionica;
        public AddEditTerminWindow(Termin termin, Ustanova ustanova, UserWindowState userWindowState = UserWindowState.Addition)
        {
            InitializeComponent();

            Ustanova = ustanova;
            ubaciUcioniceUCombo();

            TipNastaveCombo.Items.Add("Predavanja");
            TipNastaveCombo.Items.Add("Vjezbe");

            DanCombo.Items.Add("Ponedjeljak");
            DanCombo.Items.Add("Utorak");
            DanCombo.Items.Add("Srijeda");
            DanCombo.Items.Add("Cetvrtak");
            DanCombo.Items.Add("Petak");
            DanCombo.Items.Add("Subota");
            DanCombo.Items.Add("Nedjelja");

            Ustanova = ustanova;

            state = userWindowState;
            Termin = termin;
            this.DataContext = Termin;
        }

        public AddEditTerminWindow(Termin termin, Ucionica ucionica, UserWindowState userWindowState = UserWindowState.Addition)
        {
            InitializeComponent();
            lblUcionica.Visibility = Visibility.Hidden;
            UcioniceCombo.Visibility = Visibility.Hidden;
            Ucionica = ucionica;


            TipNastaveCombo.Items.Add("Predavanja");
            TipNastaveCombo.Items.Add("Vjezbe");

            DanCombo.Items.Add("Ponedjeljak");
            DanCombo.Items.Add("Utorak");
            DanCombo.Items.Add("Srijeda");
            DanCombo.Items.Add("Cetvrtak");
            DanCombo.Items.Add("Petak");
            DanCombo.Items.Add("Subota");
            DanCombo.Items.Add("Nedjelja");


            state = userWindowState;
            Termin = termin;
            this.DataContext = Termin;
        }

        public void ubaciUcioniceUCombo()
        {
            foreach (Ucionica ucionica in Ustanova.Ucionice)
            {
                if (ucionica.Active == true)
                {
                    UcioniceCombo.Items.Add(ucionica.BrojUcionice);
                }
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void SaveTerminBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Validacija() == false)
            {
                return;
            }
            string selectedTipNastave = (string)TipNastaveCombo.SelectedItem;
            TipNastave tipNastave;
            if (selectedTipNastave != null)
            {

                Enum.TryParse(selectedTipNastave, out tipNastave);

                Termin.TipNastave = tipNastave;
            }

            string selectedDan = (string)DanCombo.SelectedItem;
            Dani dan;
            if (selectedDan != null)
            {
                Enum.TryParse(selectedDan, out dan);

                Termin.Dan = dan;
            }
            string selectedUcionica = (string)UcioniceCombo.SelectedItem;
            if (selectedUcionica != null)
            {
                Ucionica ucionica = (Ucionica)Util.GetUcionicabyBroj(UcioniceCombo.Text);
                Korisnik korisnik = Util.prijavljeniKorisnik;

                Termin termin = new Termin((DateTime)Termin.TerminOd, (DateTime)Termin.TerminDo, (Dani)DanCombo.SelectedItem, (TipNastave)TipNastaveCombo.SelectedItem, ucionica, korisnik, true);
            }
            

            DialogResult = true;
            Close();
        }

        public bool Validacija()
        {
            if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Administrator"))
            {
                if (UcioniceCombo.SelectedItem == null)
                {
                    MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                    return false;
                }
            }
            if (TipNastaveCombo.SelectedItem == null || TerminOd.Text.Equals("") || TerminDo.Text.Equals("") || DanCombo.SelectedItem == null)
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }
    }
}
