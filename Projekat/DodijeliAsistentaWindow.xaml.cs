﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for DodijeliAsistentaWindow.xaml
    /// </summary>
    public partial class DodijeliAsistentaWindow : Window
    {
        public Asistent Asistent;
        public DodijeliAsistentaWindow()
        {
            InitializeComponent();
            ubaciAsistenteUCombo();
        }

        public void ubaciAsistenteUCombo()
        {
            AsistentCombo.Items.Clear();
            foreach (Asistent asistent in Util.Asistenti)
            {
                string KImeProf = KorisnikDAO.GetProf(asistent.KorisnickoIme);
                Console.WriteLine("IME PROFESORA: " + KImeProf + "USTANOVA asistenta: " + asistent.Ustanova.sifra + "USTANOVA PRIJAVLJENJOG: " + Util.prijavljeniKorisnik.Ustanova.sifra);
                if (KImeProf == null && asistent.Active == true && asistent.Ustanova.sifra == Util.prijavljeniKorisnik.Ustanova.sifra)
                {
                    Console.WriteLine("asistent: " + asistent.KorisnickoIme);
                    AsistentCombo.Items.Add(asistent.KorisnickoIme);
                }
            }
        }

        public bool Validacija()
        {
            if (AsistentCombo.SelectedItem == null)
            {
                MessageBox.Show($"Niste odabrali asistenta!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void SaveAsistentBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Validacija() == false)
            {
                return;
            }
            Asistent asistent = (Asistent)Util.GetAsistentbyKIme(AsistentCombo.Text);
            Asistent = asistent;
            Profesor profesor = (Profesor)Util.GetProfbyKIme(Util.prijavljeniKorisnik.KorisnickoIme);
            asistent.Profesor = profesor;
            KorisnikDAO korisnikDao = new KorisnikDAO();
            korisnikDao.UpdateAsistent(asistent);

            DialogResult = true;
            Close();
        }

    }
}
