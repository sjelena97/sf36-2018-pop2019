﻿using Projekat.DAO;
using Projekat.Exceptions;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Vezbe.Model;

namespace Vezbe
{
    public class Util
    {
        public static string ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Ustanova;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static Util instance;

        public static List<Korisnik> Korisnici;
        public static List<Profesor> Profesori;
        public static List<Asistent> Asistenti;
        public static List<Ustanova> Ustanove = new List<Ustanova>();
        public static List<Termin> Termini = new List<Termin>();
        //        public static List<Ucionica> Ucionice = new List<Ucionica>();
        public static Korisnik prijavljeniKorisnik;
        private Util() { }

        public static Util GetInstance()
        {
            if (instance == null)
            {
                instance = new Util();
            }

            return instance;
        }
        public static Korisnik LogIn(String korisnickoIme, String lozinka)
        {
            foreach (Korisnik korisnik in Korisnici)
            {
                if (korisnik.KorisnickoIme == korisnickoIme && korisnik.Lozinka == lozinka && korisnik.Active == true)
                {
                    return korisnik;
                }
            }
            return null;
        }
        public static List<Korisnik> GetZaposleni(Ustanova ustanova)
        {
            List<Korisnik> zaposleni = new List<Korisnik>();
            foreach (Profesor profesor in Profesori)
            {
                if (profesor.Ustanova.sifra == ustanova.sifra)
                {
                    zaposleni.Add(profesor);
                }
            }
            foreach (Asistent asistent in Asistenti)
            {
                if (asistent.Ustanova.sifra == ustanova.sifra)
                {
                    zaposleni.Add(asistent);
                }
            }
            return zaposleni;
        }

        public static List<Korisnik> GetAdmini()
        {
            List<Korisnik> admini = new List<Korisnik>();
            foreach (Korisnik korisnik in Korisnici)
            {
                if (korisnik.TipKorisnika.Equals(TipKorisnika.Administrator))
                {
                    admini.Add(korisnik);
                }
            }

            return admini;
        }

        public static void GetProfesori()
        {
            Profesori = new List<Profesor>();
            foreach (Korisnik korisnik in Korisnici)
            {
                if (korisnik.TipKorisnika.ToString().Equals("Profesor"))
                {
                    Profesor profesor = new Profesor(korisnik.Ime, korisnik.Prezime, korisnik.Email, korisnik.KorisnickoIme, korisnik.Lozinka, korisnik.Active);
                    profesor.Ustanova = korisnik.Ustanova;
                    Console.WriteLine(profesor.Ime + " je ime profe");
                    Profesori.Add(profesor);
                }
            }

        }

        public static void GetAsistenti()
        {
            Asistenti = new List<Asistent>();
            foreach (Korisnik korisnik in Korisnici)
            {
                if (korisnik.TipKorisnika.ToString().Equals("Asistent"))
                {
                    string KImeProf = KorisnikDAO.GetProf(korisnik.KorisnickoIme);
                    Profesor prof = GetProfbyKIme(KImeProf);
                    Asistent asistent = new Asistent(korisnik.Ime, korisnik.Prezime, korisnik.Email, korisnik.KorisnickoIme, korisnik.Lozinka, prof, korisnik.Active);
                    asistent.Ustanova = korisnik.Ustanova;
                    asistent.Profesor = prof;
                    if (prof != null)
                    {
                        prof.Asistenti.Add(asistent);
                        Asistenti.Add(asistent);
                    }
                    else
                    {
                        Asistenti.Add(asistent);
                    }
                }
            }

        }

        public static List<Asistent> GetAsistenti(Profesor profesor)
        {
            List<Asistent> asistentiProfesora = new List<Asistent>();
            foreach (Korisnik korisnik in Korisnici)
            {
                string KImeProf = KorisnikDAO.GetProf(profesor.KorisnickoIme);
                Profesor prof = GetProfbyKIme(KImeProf);
                Asistent asistent = new Asistent(korisnik.Ime, korisnik.Prezime, korisnik.Email, korisnik.KorisnickoIme, korisnik.Lozinka, prof, korisnik.Active);
                asistent.Ustanova = korisnik.Ustanova;
                asistent.Profesor = prof;

            }

            return asistentiProfesora;

        }

        public static Profesor GetProfbyKIme(String KIme)
        {
            foreach (Profesor profesor in Profesori)
            {
                if (profesor.KorisnickoIme.Equals(KIme))
                {
                    return profesor;
                }
            }
            return null;
        }

        public static Asistent GetAsistentbyKIme(String KIme)
        {
            foreach (Asistent asistent in Asistenti)
            {
                if (asistent.KorisnickoIme.Equals(KIme))
                {
                    return asistent;
                }
            }
            return null;
        }

        /*       public void initKorisnici()
               {
                   Korisnik korisnik1 = new Korisnik();
                   korisnik1.Id = 1;
                   korisnik1.Ime = "Jedan";
                   korisnik1.Prezime = "Jedan";
                   korisnik1.Email = "email1@hotmail.com";
                   korisnik1.KorisnickoIme = "Korisnik 1";
                   korisnik1.Lozinka = "password";
                   korisnik1.TipKorisnika = TipKorisnika.Administrator;

                   Korisnici.Add(korisnik1);

                   Korisnik korisnik2 = new Korisnik();
                   korisnik2.Id = 2;
                   korisnik2.Ime = "Dva";
                   korisnik2.Prezime = "Dva";
                   korisnik2.Email = "email2@hotmail.com";
                   korisnik2.KorisnickoIme = "Korisnik 2";
                   korisnik2.Lozinka = "password2";
                   korisnik2.TipKorisnika = TipKorisnika.Profesor;

                   Korisnici.Add(korisnik2);

                   Korisnik korisnik3 = new Korisnik();
                   korisnik3.Id = 3;
                   korisnik3.Ime = "Tri";
                   korisnik3.Prezime = "Tri";
                   korisnik3.Email = "email3@hotmail.com";
                   korisnik3.KorisnickoIme = "Korisnik 3";
                   korisnik3.Lozinka = "password3";
                   korisnik3.TipKorisnika = TipKorisnika.Asistent;

                   Korisnici.Add(korisnik3);
               }

               public void initUstanove()
               {
                   Ustanova ustanova1 = new Ustanova();
                   ustanova1.sifra = 1;
                   ustanova1.naziv = "Ustanova 1";
                   ustanova1.lokacija = "Novi Sad bb";
                   ustanova1.ucionice = new List<Ucionica>();

                   Ustanove.Add(ustanova1);

                   Ustanova ustanova2 = new Ustanova();
                   ustanova2.sifra = 2;
                   ustanova2.naziv = "Ustanova 2";
                   ustanova2.lokacija = "Novi Sad bb";
                   ustanova2.ucionice = new List<Ucionica>();

                   Ustanove.Add(ustanova2);

                   Ustanova ustanova3 = new Ustanova();
                   ustanova3.sifra = 3;
                   ustanova3.naziv = "Ustanova 3";
                   ustanova3.lokacija = "Novi Sad bb";
                   ustanova3.ucionice = new List<Ucionica>();

                   Ustanove.Add(ustanova3);
               }

               public void initUcionice()
               {
                 Ucionica ucionica1 = new Ucionica();
       //            ucionica1.Ustanova = GetUstanova(1);
                   ucionica1.Sifra = 1;
                   ucionica1.BrojUcionice = "1";
                   ucionica1.BrojMesta = 30;
                   ucionica1.TipUcionice = TipUcionice.Racunarska;

                   Ucionice.Add(ucionica1);

                   Ucionica ucionica2 = new Ucionica();
       //            ucionica2.Ustanova = GetUstanova(2);
                   ucionica2.Sifra = 2;
                   ucionica2.BrojUcionice = "2";
                   ucionica2.BrojMesta = 40;
                   ucionica2.TipUcionice = TipUcionice.Bezracunarska;

                   Ucionice.Add(ucionica2);

                   Ucionica ucionica3 = new Ucionica();
       //            ucionica3.Ustanova = GetUstanova(3);
                   ucionica3.Sifra = 3;
                   ucionica3.BrojUcionice = "3";
                   ucionica3.BrojMesta = 20;
                   ucionica3.TipUcionice = TipUcionice.Racunarska;

                   Ucionice.Add(ucionica3);

                   Ucionica ucionica4 = new Ucionica();
       //            ucionica4.Ustanova = GetUstanova(3);
                   ucionica4.Sifra = 4;
                   ucionica4.BrojUcionice = "4";
                   ucionica4.BrojMesta = 50;
                   ucionica4.TipUcionice = TipUcionice.Bezracunarska;

                   Ucionice.Add(ucionica4);
               }
               public void WriteUsersToFile()
               {
                   IFormatter formatter = new BinaryFormatter();
                   Stream outputStream = new FileStream(@"../../Resources/korisnici.bin", FileMode.Create, FileAccess.Write);

                   formatter.Serialize(outputStream, Korisnici);
                   outputStream.Close();
               }

               public void WriteUstanoveToFile()
               {
                   IFormatter formatter = new BinaryFormatter();
                   Stream outputStream = new FileStream(@"../../Resources/ustanove.bin", FileMode.Create, FileAccess.Write);

                   formatter.Serialize(outputStream, Ustanove);
                   outputStream.Close();
               }

               public void WriteUcioniceToFile()
               {
                   IFormatter formatter = new BinaryFormatter();
                   Stream outputStream = new FileStream(@"../../Resources/ucionice.bin", FileMode.Create, FileAccess.Write);

                   formatter.Serialize(outputStream, Ucionice);
                   outputStream.Close();
               }

               public void ReadUsersFromFile()
               {
                   IFormatter formatter = new BinaryFormatter();
                   Stream inputStream = new FileStream(@"../../Resources/korisnici.bin", FileMode.Open, FileAccess.Read);

                   Korisnici = (List<Korisnik>)formatter.Deserialize(inputStream);
                   inputStream.Close();

               }
               public void ReadUcioniceFromFile()
               {
                   IFormatter formatter = new BinaryFormatter();
                   Stream inputStream = new FileStream(@"../../Resources/ucionice.bin", FileMode.Open, FileAccess.Read);

                   Ucionice = (List<Ucionica>)formatter.Deserialize(inputStream);
                   inputStream.Close();

               }

               public void ReadUstanoveFromFile()
               {
                   IFormatter formatter = new BinaryFormatter();
                   Stream inputStream = new FileStream(@"../../Resources/ustanove.bin", FileMode.Open, FileAccess.Read);

                   Ustanove = (List<Ustanova>)formatter.Deserialize(inputStream);
                   inputStream.Close();

               }

        public Korisnik GetKorisnik(string kIme )
        {
            foreach(var korisnik in Korisnici)
            {
                if(korisnik.KorisnickoIme == kIme)
                {
                    return korisnik;
                }
            }

            return null;

        }

        public Ustanova GetUstanova(int sifra)
        {
            foreach (var ustanova in Ustanove)
            {
                if (ustanova.sifra == sifra)
                {
                    return ustanova;
                }
            }

            return null;

        }

        public Ucionica GetUcionica(int sifra)
        {
            foreach (var ucionica in Ucionice)
            {
                if (ucionica.Sifra == sifra)
                {
                    return ucionica;
                }
            }

            return null;

        }

        public List<Ucionica> GetUcionice(Ustanova ustanova)
        {
            List<Ucionica> nadjene = new List<Ucionica>();

            foreach (var ucionica in Ucionice)
            {
//                if (ucionica.Ustanova.Equals(ustanova))
                {
                    nadjene.Add(ucionica);
                }
            }

            return nadjene;
        }*/

        public List<Korisnik> GetKorisnici(string KorisnickoIme)
        {
            List<Korisnik> nadjeni = new List<Korisnik>();

            foreach (var korisnik in Korisnici)
            {
                if (korisnik.KorisnickoIme.Contains(KorisnickoIme))
                {
                    nadjeni.Add(korisnik);
                }
            }

            return nadjeni;
        }

        public List<Ustanova> GetUstanove(string naziv)
        {
            List<Ustanova> nadjene = new List<Ustanova>();

            foreach (var ustanova in Ustanove)
            {
                if (ustanova.naziv.Contains(naziv))
                {
                    nadjene.Add(ustanova);
                }
            }

            return nadjene;
        }

        /*       public List<Ucionica> GetUcionice(TipUcionice tipUcionice)
               {
                   List<Ucionica> nadjene = new List<Ucionica>();

                   foreach (var ucionica in Ucionice)
                   {
                       if (ucionica.TipUcionice == tipUcionice)
                       {
                           nadjene.Add(ucionica);
                       }
                   }

                   return nadjene;
               }
               public void CreateKorisnik(Korisnik k)
               {
                   string poruka = "";
                   foreach(var korisnik in Korisnici)
                   {
                       if(korisnik.Email == k.Email)
                       {
                           poruka += " Korisnik sa tim email-om vec postoji";
                       }
                       if (korisnik.KorisnickoIme == k.KorisnickoIme)
                       {
                           poruka += " Korisnik sa tim korisnickim imenom vec postoji ";
                       }
                   }

                   if (poruka != "")
                   {
                       throw new UserException(poruka);
                   }
                   Korisnici.Add(k);

               }

               public void UpdateKorisnik(Korisnik original, Korisnik copy)
               {
                   int index = Korisnici.IndexOf(original);
                   Korisnici[index] = copy;


               }

               public void Update(Korisnik k)
               {
                   for(int i = 0; i<Korisnici.Count; i++)
                   {
                       if(Korisnici.ElementAt(i).KorisnickoIme == k.KorisnickoIme)
                       {
                           Korisnici[i] = k;
                           break;
                       }
                   }
               }

               public void DeleteKorisnik (Korisnik k)
               {
                   //Korisnici.Remove(k);
                   int i = 0;
                   for (; Korisnici.Count>i; i++)
                   {
                       if(Korisnici.ElementAt(i).KorisnickoIme == k.KorisnickoIme)
                       {
                           //Korisnici.RemoveAt(i);
                           Korisnici.ElementAt(i).Active = false;
                           break;
                       }
                   }
               }

               public void SortUsersByKIme()
               {
                   Korisnici.OrderBy(u => u.KorisnickoIme);
               }

               public void SortUsersByEmail()
               {
                   Korisnici.OrderBy(u => u.Email);
               }*/


        /*       public List<Korisnik> Sort()
               {
                   List<Korisnik> Sortirana = new List<Korisnik>();
                   int velicinaList = Korisnici.Count;
                   for(int i = 0; i<velicinaList; i++)
                   {
                       var temp = Korisnici.ElementAt(1);

                       //5 4 
                       //4 5 

                       //5 4
                       //temp = 4
                       //5 5
                       //4 5 

                       int prvi = Korisnici.ElementAt(5).KorisnickoIme;
                       if (Korisnici.ElementAt(i).Id > Korisnici.ElementAt(i+1).Id)
                       {
                           //prvi = Korisnici.ElementAt(i+1);

                       }
                   }
                   return Korisnici;
               }

               public void CreateUstanova(Ustanova u)
               {
                   string poruka = "";
                   foreach (var ustanova in Ustanove)
                   {
                       if (ustanova.sifra == u.sifra)
                       {
                           poruka += "Ustanova sa tom sifrom vec postoji";
                       }
                   }

                   if (poruka != "")
                   {
                       throw new UserException(poruka);
                   }
                   Ustanove.Add(u);

               }

               public void UpdateUstanova(Ustanova original, Ustanova copy)
               {
                   int index = Ustanove.IndexOf(original);
                   Ustanove[index] = copy;


               }

               public void Update(Ustanova u)
               {
                   for (int i = 0; i < Ustanove.Count; i++)
                   {
                       if (Ustanove.ElementAt(i).sifra == u.sifra)
                       {
                           Ustanove[i] = u;
                           break;
                       }
                   }
               }

               public void DeleteUstanova(Ustanova u)
               {
                   int i = 0;
                   for (; Ustanove.Count > i; i++)
                   {
                       if (Ustanove.ElementAt(i).sifra == u.sifra)
                       {
                           Ustanove.ElementAt(i).Active = false;
                           break;
                       }
                   }
               }

               public void SortUstanoveBySifra()
               {
                   Ustanove.OrderBy(u => u.sifra);
               }

               public void SortUstanoveByNaziv()
               {
                   Ustanove.OrderBy(u => u.naziv);
               }

               public void SortUstanoveByLokacija()
               {
                   Ustanove.OrderBy(u => u.lokacija);
               }

               /*       public void CreateUcionica(Ucionica u)
                      {
                          string poruka = "";
                          foreach (var ucionica in Ucionice)
                          {
                              if (ucionica.Sifra == u.Sifra)
                              {
                                  poruka += "Ucionica sa tom sifrom vec postoji";
                              }
                              if (ucionica.BrojUcionice == u.BrojUcionice)
                              {
                                  poruka += " Ucionica sa tim brojem vec postoji";
                              }
                          }

                          if (poruka != "")
                          {
                              throw new UserException(poruka);
                          }
                          Ucionice.Add(u);

                      }

                      public void UpdateUcionica(Ucionica original, Ucionica copy)
                      {
                          int index = Ucionice.IndexOf(original);
                          Ucionice[index] = copy;


                      }

                      public void Update(Ucionica u)
                      {
                          for (int i = 0; i < Ucionice.Count; i++)
                          {
                              if (Ucionice.ElementAt(i).Sifra == u.Sifra)
                              {
                                  Ucionice[i] = u;
                                  break;
                              }
                          }
                      }

                      public void DeleteUcionica(Ucionica u)
                      {
                          int i = 0;
                          for (; Ucionice.Count > i; i++)
                          {
                              if (Ucionice.ElementAt(i).Sifra == u.Sifra)
                              {
                                  Ucionice.ElementAt(i).Active = false;
                                  break;
                              }
                          }
                      }

                      public void SortUcioniceBySifra()
                      {
                          Ucionice.OrderBy(u => u.Sifra);
                      }

                      public void SortUcioniceByBrojUcionice()
                      {
                          Ucionice.OrderBy(u => u.BrojUcionice);
                      }

                      public void SortUcioniceByBrojMesta()
                      {
                          Ucionice.OrderBy(u => u.BrojMesta);
                      }*/


        public static Ucionica NadjiUcionicuPoBr(String BrojUcionice, Ustanova ustanova)
        {
            foreach (Ucionica ucionica in ustanova.Ucionice)
            {
                if (ucionica.BrojUcionice.Equals(BrojUcionice) && ucionica.Active == true)
                {
                    return ucionica;
                }
            }
            return null;
        }

        public static Ucionica PostojiUcionicaUustanovi(String BrojUcionice, Ustanova ustanova)
        {
            foreach (Ucionica ucionica in ustanova.Ucionice)
            {
                if (ucionica.BrojUcionice.Equals(BrojUcionice) && ucionica.Active == true)
                {
                    return ucionica;
                }
            }
            return null;
        }

        public static Ucionica PostojiUcionicaUustanoviUpdate(String BrojUcionice, Ustanova ustanova, Ucionica ucionica)
        {
            foreach (Ucionica u in ustanova.Ucionice)
            {
                if (u.BrojUcionice.Equals(BrojUcionice) && u.Active == true && u.Sifra != ucionica.Sifra)
                {
                    return u;
                }
            }
            return null;
        }

        public static Korisnik PostojiliKorisnickoIme(String kIme)
        {
            foreach (Korisnik korisnik in Korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(kIme) && korisnik.Active == true)
                {
                    return korisnik;
                }
            }
            return null;
        }

        public static Korisnik PostojiliKorisnickoImeUpdate(String KIme, Korisnik korisnik)
        {
            foreach (Korisnik k in Korisnici)
            {
                if (k.KorisnickoIme.Equals(KIme) && k.Active == true && k.KorisnickoIme != korisnik.KorisnickoIme)
                {
                    return k;
                }
            }
            return null;
        }

        public static Ustanova GetUstanovabyNaziv(String naziv)
        {
            foreach (Ustanova ustanova in Ustanove)
            {
                if (ustanova.naziv.Equals(naziv) && ustanova.Active == true)
                {
                    return ustanova;
                }
            }
            return null;
        }

        public static Ucionica GetUcionicabyBroj(String broj)
        {
            foreach (Ustanova ustanova in Ustanove)
            {
                foreach(Ucionica u in ustanova.Ucionice)
                {
                    if (u.BrojUcionice.Equals(broj) && u.Active == true)
                    {
                        return u;
                    }
                }
            }
            return null;
        }

        /*       public static void PretragaUstanova(string s, string parametar)
               {
                   List<Ustanova> ustanoveZaPretragu = new List<Ustanova>();
                   ustanoveZaPretragu.Clear();
                   foreach (Ustanova ustanova in Ustanove)
                   {
                       if(parametar.Equals("Naziv"))
                       {
                           if ((ustanova.naziv.ToString().ToLower()).Contains(s.ToString().ToLower()))
                           {
                               Console.WriteLine(ustanova.naziv + " naziv");
                               ustanoveZaPretragu.Add(ustanova);
                           }
                       }else if(parametar.Equals("Lokacija"))
                       {
                           if (ustanova.lokacija.Contains(s))
                           {
                               ustanoveZaPretragu.Add(ustanova);
                           }
                       }
                   }
                   if (s != "")
                   {
                       Util.Ustanove.Clear();
                       foreach(Ustanova u in ustanoveZaPretragu)
                       {
                           Util.Ustanove.Add(u);
                       }
                   }
               }
               */

    }
}
