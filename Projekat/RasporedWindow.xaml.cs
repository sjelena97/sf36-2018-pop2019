﻿using Projekat.DAO;
using Projekat.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vezbe;
using Vezbe.Model;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for RasporedWindow.xaml
    /// </summary>
    public partial class RasporedWindow : Window
    {
        public Ustanova ustanovaZaRaspored;
        public ObservableCollection<Termin> terminiZaRaspored = new ObservableCollection<Termin>();
        public ICollectionView CollectionView;
        public List<Termin> terminiKorisnika = new List<Termin>();
        public Korisnik prijavljeni;
        public RasporedWindow(Ustanova ustanova)
        {
            InitializeComponent();
            GlavniZaUseraBtn.Visibility = Visibility.Hidden;
            foreach (Ustanova u in Util.Ustanove)
            {
                if (ustanova.sifra == u.sifra)
                {
                    Console.WriteLine("naslo ustanovu");
                    foreach(Ucionica uc in u.Ucionice)
                    {
                        Console.WriteLine("ima ucionice");
                        foreach (Termin t in uc.termini)
                        {
                            Console.WriteLine("Ima termine");
                            terminiZaRaspored.Add(t);
                        }
                    }
                }
            }
            ustanovaZaRaspored = ustanova;
            ubaciParametreUCombo();

            CollectionView = CollectionViewSource.GetDefaultView(terminiZaRaspored);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
        }

        public RasporedWindow(Korisnik korisnik)
        {

            prijavljeni = korisnik;
            TerminDAO terminDao = new TerminDAO();
            terminiKorisnika = terminDao.SelectTermini(korisnik);
            InitializeComponent();
            ubaciParametreUCombo();
            CollectionView = CollectionViewSource.GetDefaultView(terminiKorisnika);
            CollectionView.Filter = ActiveFilter;
            DataContext = CollectionView;
        }

        private void GlavniZaUseraBtn_Click(object sender, RoutedEventArgs e)
        {

            Close();

            TerminiWindow terminiWindow = new TerminiWindow();
            terminiWindow.ShowDialog();

        }


        private bool ActiveFilter(object obj)
        {
            var termin = obj as Termin;
            return termin.Active;
        }

        private void PretragaDanDropDownClosed(object sender, EventArgs e)
        {
            String parametar = "";
            if (ComboPretragaDan.SelectedItem != null)
            {
                parametar = ComboPretragaDan.SelectedItem.ToString();
                if(Util.prijavljeniKorisnik.KorisnickoIme == "")
                {
                    TerminDAO.PretragaTerminaZaRaspored(parametar, terminiZaRaspored, ustanovaZaRaspored);
                }
                else
                {
                    if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Profesor") || Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Asistent"))
                    {
                        TerminDAO.PretragaTerminaZaRasporedKorisnika(parametar, terminiKorisnika, prijavljeni);
                    }
                    else if (Util.prijavljeniKorisnik.TipKorisnika.ToString().Equals("Administrator"))
                    {
                        TerminDAO.PretragaTerminaZaRaspored(parametar, terminiZaRaspored, ustanovaZaRaspored);
                    }

                }
                
                CollectionView.Refresh();
            }

        }

        public void ubaciParametreUCombo()
        {
            ComboPretragaDan.Items.Add("Ponedjeljak");
            ComboPretragaDan.Items.Add("Utorak");
            ComboPretragaDan.Items.Add("Srijeda");
            ComboPretragaDan.Items.Add("Cetvrtak");
            ComboPretragaDan.Items.Add("Petak");
            ComboPretragaDan.Items.Add("Subota");
            ComboPretragaDan.Items.Add("Nedjelja");
        }
    }
}
