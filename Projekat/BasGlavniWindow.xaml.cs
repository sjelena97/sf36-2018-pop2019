﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for BasGlavniWindow.xaml
    /// </summary>
    public partial class BasGlavniWindow : Window
    {
        public BasGlavniWindow()
        {
            InitializeComponent();
        }

        private void NeregistrovaniBtn_Click(object sender, RoutedEventArgs e)
        {
            NeregistrovaniWindow neregistrovani = new NeregistrovaniWindow();
            neregistrovani.Show();
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
        }

        /*        private void KorisniciBtn_Click(object sender, RoutedEventArgs e)
                {
                    MainWindow WindowUsers = new MainWindow();
                    WindowUsers.Show();
                }

                private void UstanoveBtn_Click(object sender, RoutedEventArgs e)
                {
                    UstanoveWindow WindowUstanove = new UstanoveWindow();
                    WindowUstanove.Show();
                }*/

        //        private void UcioniceBtn_Click(object sender, RoutedEventArgs e)
        //        {
        //            UcioniceWindow WindowUcionice = new UcioniceWindow();
        //            WindowUcionice.Show();
        //        }
    }
}
