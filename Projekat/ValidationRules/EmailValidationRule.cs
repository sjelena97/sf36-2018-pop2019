﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Projekat.ValidationRules
{
    public class EmailValidationRule : ValidationRule
    {
        Regex regex = new Regex(@"\b[A-Z0-9]{1,10}@[A-Z]{1,10}\.[A-Z]{2,3}\b", RegexOptions.IgnoreCase);
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string email = value as string;

            if (email == null || email.Equals(string.Empty))
                return new ValidationResult(false, "Polje korisnicko ime je prazno!");
            else if (regex.Match(email).Success)
                return new ValidationResult(true, null);
            return new ValidationResult(false, "Pogresan format!");
        }
    }
}
